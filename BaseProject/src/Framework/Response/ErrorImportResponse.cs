﻿using VNP.Salary.Framework.Enums;

namespace VNP.Salary.Framework.Response
{

    public class ErrorImportResponse
    {
        public string SheetName { get; set; }
        public int RowIndex { get; set; }
        public string code { get; set; }
        public EnumImportErrorCode StatusCode { get; set; }
        public string Message { get; set; }
    }

}
