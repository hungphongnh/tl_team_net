﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace VNP.Salary.Framework.Redis
{
    public interface IDistributedCacheMaster : IDistributedCache
    {
    }

    public interface IDistributedCacheSlave : IDistributedCache
    {
    }

    public class RedisCacheOptionsMaster : RedisCacheOptions
    {
    }

    public class RedisCacheOptionsSlave : RedisCacheOptions
    {
    }

    public class RedisCacheMaster : RedisCache, IDistributedCacheMaster
    {
        public RedisCacheMaster(IOptions<RedisCacheOptionsMaster> optionsAccessor) : base(optionsAccessor)
        {
        }
    }

    public class RedisCacheSlave : RedisCache, IDistributedCacheSlave
    {
        public RedisCacheSlave(IOptions<RedisCacheOptionsSlave> optionsAccessor) : base(optionsAccessor)
        {
        }
    }
}
