﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using VNP.Salary.Framework.Enums;

namespace VNP.Salary.Framework.Extension
{
    public static class ExtensionMoney
    {
        public static string VNNumberFormat(string number)
        {
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
            double convert = number == null ? 0 : Convert.ToDouble(number);
            return convert == 0 ? "0" : convert.ToString("#,###", cul.NumberFormat);
        }
        public static double StringToDouble(string number)
        {
            double convert = number == null ? 0 : Convert.ToDouble(number);
            return convert;
        }
    }
}