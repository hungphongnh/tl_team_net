﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Reflection;
using VNP.Salary.Framework.Config;
using static VNP.Salary.Framework.Enums.EnumCommon;

namespace VNP.Salary.Framework.Extension
{

    public class ExtensionExcel<TEntity> where TEntity : class
    {
        /// <summary>
        /// create row for masterdata template
        /// </summary>
        /// <param name="masterDataSheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="addressCol"></param>
        /// <param name="listData"></param>
        public ExtensionExcel(int renderType, ref ISheet masterDataSheet, ref int rowIndex, int addressCol, List<TEntity> listData)
        {
            if (listData != null && listData.Count > 0)
            {
                int createRowCount = listData.Count - rowIndex + 2;
                if (createRowCount > 0)
                {
                    for (var i = 0; i < createRowCount; i++)
                    {
                        var nextIndexToCreate = rowIndex + i;
                        var itemRow = masterDataSheet.GetRow(nextIndexToCreate);
                        if (itemRow == null) masterDataSheet.CreateRow(nextIndexToCreate);
                        var row = masterDataSheet.GetRow(nextIndexToCreate);
                        for (int j = 0; j < MasterDataExcel.ColCount; j++)
                        {
                            row.CreateCell(j);
                        }
                    }
                    rowIndex += createRowCount;
                }

                int index = 2;
                foreach (var model in listData)
                {
                    Type t = model.GetType();
                    PropertyInfo property = t.GetProperty("Value");
                    object firstValue = null, secondValue = null;

                    switch (renderType)
                    {
                        case EnumExcelRenderType.PARAMATER:
                            object value = property.GetValue(model);
                            if (value != null)
                            {
                                property = t.GetProperty("Description");
                                firstValue = value;
                                secondValue = property.GetValue(model);
                            }
                            break;
                        case EnumExcelRenderType.INDENTED_TITLE:
                            property = t.GetProperty("IndentedTitle");
                            if (property != null)
                            {
                                secondValue = property.GetValue(model);
                                property = t.GetProperty("Id");
                                object id = property.GetValue(model);
                                firstValue = id;
                            }
                            break;
                        case EnumExcelRenderType.CODE:
                            property = t.GetProperty("Code");
                            if (property != null)
                            {
                                secondValue = property.GetValue(model);
                                property = t.GetProperty("Id");
                                object id = property.GetValue(model);
                                firstValue = id;
                            }
                            break;
                        case EnumExcelRenderType.DUPLICATE_CODE:
                            property = t.GetProperty("Code");
                            if (property != null)
                            {
                                secondValue = property.GetValue(model);
                                firstValue = secondValue;
                            }
                            break;
                        case EnumExcelRenderType.CODETYPE_CODE:
                            property = t.GetProperty("Code");
                            if (property != null)
                            {
                                secondValue = property.GetValue(model);
                                property = t.GetProperty("CodeType");
                                var codeType = property.GetValue(model);
                                firstValue = codeType;
                            }
                            break;
                        default:
                            property = t.GetProperty("Name");
                            if (property != null)
                            {
                                secondValue = property.GetValue(model);
                                property = t.GetProperty("Id");
                                object id = property.GetValue(model);
                                firstValue = id;
                            }
                            break;

                    }
                    #region old
                    //if (property != null)
                    //{
                    //    object value = property.GetValue(model);
                    //    if (value != null)
                    //    {
                    //        property = t.GetProperty("Description");
                    //        firstValue = value;
                    //        secondValue = property.GetValue(model);
                    //    }
                    //}
                    //else
                    //{
                    //    property = t.GetProperty("IndentedTitle");
                    //    if (property != null)
                    //    {
                    //        secondValue = property.GetValue(model);
                    //        property = t.GetProperty("Id");
                    //        object id = property.GetValue(model);
                    //        firstValue = id;
                    //    }
                    //    else
                    //    {
                    //        property = t.GetProperty("Name");
                    //        if (property != null)
                    //        {
                    //            secondValue = property.GetValue(model);
                    //            property = t.GetProperty("Id");
                    //            object id = property.GetValue(model);
                    //            firstValue = id;
                    //        }
                    //    }
                    //}
                    #endregion

                    var cellFirst = masterDataSheet.GetRow(index).GetCell(addressCol);
                    var cellSeccond = masterDataSheet.GetRow(index).GetCell(addressCol + 1);
                    if (cellFirst == null) cellFirst = masterDataSheet.GetRow(index).CreateCell(addressCol);
                    if (cellSeccond == null) cellSeccond = masterDataSheet.GetRow(index).CreateCell(addressCol + 1);
                    cellFirst.SetCellValue((firstValue != null) ? firstValue + "" : " ");
                    cellSeccond.SetCellValue(secondValue + "");

                    index++;
                }
            }
        }

    }


    public static class ExtensionExcel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static string GetCellValue(this ICell cell)
        {
            if (cell is null) return null;

            return cell.CellType switch
            {
                CellType.Blank => "",
                CellType.Unknown => "",
                CellType.Numeric => cell.NumericCellValue.ToString(),
                CellType.String => cell.StringCellValue,
                CellType.Formula => null,
                CellType.Boolean => cell.BooleanCellValue.ToString(),
                CellType.Error => null,
                _ => "",
            };
        }
    }
}