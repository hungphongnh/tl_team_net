﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace VNP.Salary.Framework.Filter
{
    [Route("api/v{version:apiVersion}/ProfessionalStaff-API/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ProfessionalStaffControllerBase : ControllerBase
    {
    }
}