﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using VNP.Salary.Framework.Enums;
using VNP.Salary.Framework.Extension;
using Dapper.Contrib.Extensions;

namespace VNP.Salary.Framework.Models
{
    public class AspNetUsersPaging: AspNetUsers
    {
        public string TenantName { get; set; }
    }
   
}