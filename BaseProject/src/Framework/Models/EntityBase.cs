﻿using System;
using System.Collections.Generic;

namespace VNP.Salary.Framework.Models
{
    public class EntityBase
    {
        public Guid Id { get; set; }
    }

    public class EntityStringBase
    {
        public string Id { get; set; }
    }

    public class EntityImport : EntityBase
    {
        public string UrlFile { get; set; }
    }

    public class ParentBase : EntityBase
    {
        public Guid? ParentId { get; set; }
    }

    public class SonBase : ParentBase
    {
        public string Name { get; set; }
    }


    public class EmployeeEntityImport : EntityImport
    {
        public bool IsCreateAccount { get; set; }
        public bool IsRandomPassword { get; set; }
        public string AccountPassword { get; set; }

        public List<Guid>? GroupIds { get; set; }

        public List<Guid>? DefaultGroupIds { get; set; }
    }

    public class TimeSheetEntityImport : EntityImport
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class TenantEntityImport : EntityImport
    {
    }

    public class Approve
    {
        public string Id { get; set; }
        public int Status { get; set; }
    }

    public class TimeImport : EntityImport
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
