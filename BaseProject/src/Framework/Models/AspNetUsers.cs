﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using VNP.Salary.Framework.Enums;
using VNP.Salary.Framework.Extension;
using Dapper.Contrib.Extensions;

namespace VNP.Salary.Framework.Models
{
    public class AspNetUsers : AuditEntity
    {
        public string Id { get; set; }
        public Guid TenantId { get; set; }

        //public string TenantName { get; set; }

        public string UserName { get; set; }

        public string NormalizedUserName { get; set; }

        public string Email { get; set; }

        public string NormalizedEmail { get; set; }

        public bool EmailConfirmed { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string ConcurrencyStamp { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTimeOffset? LockoutEnd { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }
        public string FullName { get; set; }
        public Guid EmployeeId { get; set; }
        public string EmployeeCode { get; set; }
        public int UsedState { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public DateTime? LastDateLogin { get; set; }
        public DateTime? LastChangePassword { get; set; }


        public virtual List<Claim> Claim()
        {
            if (!string.IsNullOrWhiteSpace(Id))
            {
                List<Claim> dataClaims = new List<Claim>
                {
                    new Claim (JwtRegisteredClaimNames.Jti, Guid.NewGuid ().ToString ()),
                    new Claim (JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToTimestamp().ToString(), ClaimValueTypes.Integer64),
                    new Claim (EnumClaimType.emailConfirmed , "true" ),
                    new Claim(EnumClaimType.userId, Id),
                    new Claim(EnumClaimType.tenantId, TenantId+""),
                    new Claim (EnumClaimType.userName , UserName) ,
                    new Claim (ClaimTypes.Name, UserName),
                    new Claim (EnumClaimType.isVerified, "true"),
                    new Claim (JwtRegisteredClaimNames.UniqueName, UserName),
                    new Claim (JwtRegisteredClaimNames.Sub, UserName),
                };
                return dataClaims;
            }
            return new List<Claim>();
        }
    }

}