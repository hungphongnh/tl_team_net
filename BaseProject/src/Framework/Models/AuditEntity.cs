﻿using Microsoft.AspNetCore.Http;
using System;

namespace VNP.Salary.Framework
{
    public class AuditEntity
    {
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
