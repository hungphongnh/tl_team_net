﻿using VNP.Salary.Framework.Enums;

namespace VNP.Salary.Framework.Models
{
    public class PermissionModel
    {
        public string PageName { get; set; }
        public string PermissionCode { get; set; }
    }
}
