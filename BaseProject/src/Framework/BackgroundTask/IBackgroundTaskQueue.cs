using System;
using System.Threading;
using System.Threading.Tasks;

namespace VNP.Salary.Framework.BackgroundTask
{
    public interface IBackgroundTaskQueue
    {
        void QueueBackgroundWorkItem(Func<CancellationToken, Task<HubResult>> workItem);

        Task<Func<CancellationToken, Task<HubResult>>> DequeueAsync(
            CancellationToken cancellationToken);
    }
}