using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using VNP.Salary.Framework.BackgroundTask;
using VNP.Salary.Framework.Signal;

namespace VNP.Salary.Framework.BackgroundTask
{
    public class QueuedHostedService : BackgroundService
    {
        public IBackgroundTaskQueue TaskQueue { get; }

        private readonly IHubContext<HandlerHub> _hubContext;
        private List<Func<CancellationToken, Task<HubResult>>> fail = new List<Func<CancellationToken, Task<HubResult>>>();

        public QueuedHostedService(IBackgroundTaskQueue taskQueue, IHubContext<HandlerHub> hubContext)
        {
            TaskQueue = taskQueue;
            _hubContext = hubContext;
        }

        protected async override Task ExecuteAsync(
            CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var workItem = await TaskQueue.DequeueAsync(cancellationToken);
                try
                {
                    Task task = new Task(async () =>
                    {
                        var rs = await workItem(cancellationToken);
                        await _hubContext.Clients.All.SendAsync(rs.ActionName, rs.Data);
                    });
                    task.Start();
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                // try
                // {
                //     var rs = await workItem(cancellationToken);
                //     await _hubContext.Clients.All.SendAsync(rs.ActionName, rs.Data);
                // }
                // catch (System.Exception ex)
                // {
                //     Console.WriteLine(ex.Message);
                // }
            }
        }
    }
    public class HubResult
    {
        public string ActionName { get; set; }
        public object Data { get; set; }
    }
}