﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VNP.Salary.Framework.Config
{
    public class ConfigCommon
    {
        public static Identity Identity = new Identity();
        public static string[] Domains = new string[] { "https://salary.vnpost.vn", "https://salary.vnpost.vn", "localhost:44313" };
    }

    public static class MasterDataExcel
    {
        public static string Name = "master data";
        /// <summary>
        /// 2
        /// </summary>
        public static int RowIndex = 2;
        /// <summary>
        /// 37
        /// </summary>
        public static int ColCount = 37;
        /// <summary>
        /// 0
        /// </summary>
        public static int AddressColTenant = 0;
        /// <summary>
        /// 3
        /// </summary>
        public static int AddressColGender = 3;
        /// <summary>
        /// 6
        /// </summary>
        public static int AddressColObjectCategory = 6;
        /// <summary>
        /// 9
        /// </summary>
        public static int AddressColTitle = 9;
        /// <summary>
        /// 12
        /// </summary>
        public static int AddressColJobTitle = 12;
        /// <summary>
        /// 15
        /// </summary>
        public static int AddressColJobPossition = 15;
        /// <summary>
        /// 18
        /// </summary>
        public static int AddressColIsCareerEmployee = 18;
        /// <summary>
        /// 21
        /// </summary>
        public static int AddressColStateEmployee = 21;
        /// <summary>
        /// 24
        /// </summary>
        public static int AddressColInsuranceRegion = 24;
        /// <summary>
        /// 27
        /// </summary>
        public static int AddressColTimeKeepingCatergory = 27;
        /// <summary>
        /// 33
        /// </summary>
        public static int AddressColUsedState = 33;
        /// <summary>
        /// 30
        /// </summary>
        public static int AddressColTenantLevel = 30;
        /// <summary>
        /// 45
        /// </summary>
        public static int AddressInsurranceObject = 45;
        /// <summary>
        /// 48
        /// </summary>
        public static int AddressSpecialObject = 48;
    }

    public static class ImportExcel
    {
        /// <summary>
        /// 9
        /// </summary>
        public static int RowIndex = 9;
        public static int ColIndex = 5;
        public static string CellRangeInMonth = "F{0}:AJ{1}";
        public static string TimeSheetName = "ChamCong";
        public static string TimeSheetTitle = "BẢNG THU THẬP DỮ LIỆU THÔNG TIN CHẤM CÔNG \n Tháng {0}/{1}";
        public static string ComplexCoeffcientName = "Hệ số phức tạp";
        public static string ReportName = "Báo cáo";
        public static string ComplexCoeffcientTitle = "BẢNG HỆ SỐ PHỨC TẠP \n Tháng {0}/{1}";
        public static string ReportTitle = "BẢNG TỔNG HỢP TIỀN LƯƠNG/THÙ LAO CÔNG ĐOẠN XỬ LÝ NGHIỆP VỤ {0} THÁNG {1}/{2} \n";
        public static string QuantitySheetName = "Quản lý sản lượng";
        public static string QuantitySheetTitle = "BẢNG QUẢN LÝ SẢN LƯỢNG \n Tháng {0}/{1}";
        public static string NonumberQuantitySheetName = "Import sản lượng XLNV";
        public static string NonumberQuantitySheetTitle = "NHẬP SẢN LƯỢNG XỬ LÝ NGHIỆP VỤ BƯU GỬI KHÔNG SỐ \n Tháng {0}/{1}";
        public static string ContractName = "Hợp đồng";
        public static string ContractTitle = "HỢP ĐỒNG \n Tháng {0}/{1}";
        public static int RowHeader = 6;
        public static int RowType = 8;
        public static int RowHeaderCalculateSalary = 7;
        public static string WorkingHistoryName = "Quá trình làm việc";
    }

    public static class ImportExcelTimeSheet
    {
        /// <summary>
        /// 9
        /// </summary>
        public static int ColEmpCode = 1;
        public static int ColTenantCode = 5;
        public static int ColIndex = 6; // timekeeping category[Mã chấm công] start from here
        public static int RowIndex = 9;
        public static string CellRangeInMonth = "G{0}:AK{1}";   //  change
        public static string TimeSheetName = "ChamCong";
        public static string TimeSheetTitle = "BẢNG THU THẬP DỮ LIỆU THÔNG TIN CHẤM CÔNG \n Tháng {0}/{1}";
        public static string ComplexCoeffcientName = "Hệ số phức tạp";
        public static string ComplexCoeffcientTitle = "BẢNG HỆ SỐ PHỨC TẠP \n Tháng {0}/{1}";
        public static string QuantitySheetName = "Quản lý sản lượng";
        public static string QuantitySheetTitle = "BẢNG QUẢN LÝ SẢN LƯỢNG \n Tháng {0}/{1}";
        public static string ContractName = "Hợp đồng";
        public static string ContractTitle = "HỢP ĐỒNG \n Tháng {0}/{1}";

    }

    public static class RoundNumber
    {
        public static int DigitNumberCoefficient = 3;
    }
}
