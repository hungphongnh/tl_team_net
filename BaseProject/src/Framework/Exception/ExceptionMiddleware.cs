﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Serilog.Context;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using VNP.Salary.Framework.Interface;
using VNP.Salary.Framework.Response;

namespace VNP.Salary.Framework.Exception
{
    public class ExceptionMiddleware
    {
        private StackFrame getStackFrame(System.Exception exception)
        {
            var st = new StackTrace(exception, true);
            return st.GetFrame(0);
        }

        private readonly RequestDelegate _next;
        private readonly ILoggerManager _logger;
        public ExceptionMiddleware(RequestDelegate next, ILoggerManager logger)
        {
            if (next == null) throw new ArgumentNullException(nameof(next));
            _logger = logger;
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                //await _next(httpContext);
                if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));
                var user = httpContext.User.Identity.Name ?? "anonymous";
                var client = httpContext.Connection.RemoteIpAddress.ToString() ?? "unknown";
                using (LogContext.PushProperty("User", user))
                using (LogContext.PushProperty("Client", client))
                {
                    await _next(httpContext);
                }
            }
            catch (System.Exception exception)
            {
                var stackFrame = getStackFrame(exception);
                _logger.LogError(string.Format("Exception information {0} method line {1} in {2} have message: {3}", httpContext.Request.Method, stackFrame.GetFileLineNumber(), httpContext.Request.Path, exception.Message));

                await HandleExceptionAsync(httpContext, exception);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, System.Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return context.Response.WriteAsync(
                new CustomApiResponse()
                {
                    IsError = true,
                    Result = null,
                    StatusCode = context.Response.StatusCode,
                    Message = $"{exception.Message}"
                }.ToString());
        }
    }
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}