﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace VNP.Salary.Framework.Interface
{
    public interface IRepository<T> where T : class
    {
        #region Create
        Task<Guid> Add(T entity);
        Task<string> AddBulk(IList<T> entity);
        #endregion

        #region Read
        Task<IList<T>> GetAll();
        Task<T> Get(object id);
        Task<(List<T>, long)> Paging(string name, int? usedState, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy);
        #endregion

        #region Update
        Task<bool> Update(T entity);
        Task<bool> Updates(string sSet, string sWhere);
        #endregion

        #region Delete
        Task<bool> Delete(string id);
        Task<string> Deletes(string sWhere);
        #endregion
    }
}
