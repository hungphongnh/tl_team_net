﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class FieldPrivateAttributeAPP : Attribute
    {
        private bool value = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldPrivateAttributeAPP"/> class.
        /// </summary>
        /// <param name="isPrivate">if set to <c>true</c> [is private].</param>
        public FieldPrivateAttributeAPP(bool isPrivate)
        {
            value = isPrivate;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is private.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is private; otherwise, <c>false</c>.
        /// </value>
        public bool isPrivate
        {
            get
            {
                return value;
            }
        }
    }
}
