﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class EnumServicesName
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Active")]
        public const string CORP = "";

        /// <summary>
        /// 
        /// </summary>
        [Description("ProfessionalStaff")]
        public const string ProfessionalStaff = "ProfessionalStaff";

        /// <summary>
        /// 
        /// </summary>
        [Description("Teller")]
        public const string Teller = "Teller";
    }
}
