﻿using System.ComponentModel;

namespace VNP.Salary.Framework.Enums
{
    public enum EnumImportErrorCode

    {
        [Description("File không hợp lệ!")]
        InvalidFile = 0,
        [Description("Không tìm thấy dữ liệu!")]
        NotFound = 1,
        [Description("Dữ liệu không hợp lệ!")]
        InvalidData = 2,
        [Description("Không tồn tại row trong sheet!")]
        NotExistsRow = 3
    }
}
