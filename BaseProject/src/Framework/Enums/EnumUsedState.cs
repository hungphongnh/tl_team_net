﻿using System.ComponentModel;

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class EnumUsedState
    {
        /// <summary>
        ///  Active Or Lock
        /// </summary>
        [Description("Active")]
        public const int Active = 1;

        /// <summary>
        /// 
        /// </summary>
        [Description("DeActive")]
        public const int DeActive =0;

        /// <summary>
        /// 
        /// </summary>
        [Description("Deleted")]
        public const int Deleted = -1;

        /// <summary>
        /// Approved Or Unlock
        /// </summary>
        [Description("Approved")]
        public const int Approved = 2;
    }
}
