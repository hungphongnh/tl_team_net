﻿namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class EnumFilesURL
    {
        /// <summary>
        /// 
        /// </summary>
        public const string ImportTemplate = @"/tempExcel/tenant_import_template.xlsx";
        public const string NonumberPostageTemplate = @"\tempExcel\nonumberPostage_import_template.xlsx";
        /// <summary>
        /// 
        /// </summary>
        public const string EmployeeImportTemplate = @"/tempExcel/employee_import_template.xlsx";

        /// <summary>
        /// 
        /// </summary>
        public const string TimeSheetImportTemplate = @"\tempExcel\timesheet_import_template.xlsx";

        /// <summary>
        /// 
        /// </summary>
        public const string ComplexCoefficientImportTemplate = @"\tempExcel\complex_coefficient_import_template.xlsx";

        public const string ReportImportTemplate = @"/tempExcel/report_export_template.xlsx";
        public const string ReportImportGeneralTemplate = @"/tempExcel/report_general_export_template.xlsx";

        public const string QuantityImportTemplate = @"\tempExcel\quantity_import_template.xlsx";

        public const string QuantityExportTemplate = @"\tempExcel\quantity_export_template.xlsx";

        public const string ContractImportTemplate = @"\tempExcel\contract_import_template.xlsx";

        public const string SalaryImportTemplate = @"\tempExcel\salary_import_template.xlsx";

        public const string WorkingHistoryTemplate = @"\tempExcel\working_history_template.xlsx";
    }
}
