﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VNP.Salary.Framework.Enums
{
    public class EnumClaimType
    {
        /// <summary>
        ///
        /// </summary>
        public const string DisplayName = "DisplayName";

        /// <summary>
        ///
        /// </summary>

        public const string Birthday = "Birthday";
        /// <summary>
        ///
        /// </summary>

        public const string ProvinceId = "ProvinceId";
        /// <summary>
        ///
        /// </summary>

        public const string tenantId = "tenantId";
        /// <summary>
        ///
        /// </summary>

        public const string tenantType = "tenantType";
        /// <summary>
        ///
        /// </summary>

        public const string DistrictId = "DistrictId";
        /// <summary>
        ///
        /// </summary>

        public const string PhoneNumber = "PhoneNumber";

        /// <summary>
        ///
        /// </summary>
        public const string Email = "Email";

        /// <summary>
        ///
        /// </summary>
        public const string Role = "Role";

        /// <summary>
        ///
        /// </summary>
        public const string Gender = "Gender";

        /// <summary>
        /// The email confirmed
        /// </summary>
        public const string emailConfirmed = "emailConfirmed";

        /// <summary>
        /// The user identifier/
        /// </summary>
        public const string userId = "userId";

        /// <summary>
        /// The user name
        /// </summary>
        public const string userName = "userName";

        /// <summary>
        /// Xác thực tài khoản
        /// </summary>
        public const string isVerified = "isVerified";

        /// <summary>
        /// The packages
        /// </summary>
        public const string packages = "packages";

        /// <summary>
        /// The first change password
        /// </summary>
        public const string needChangePassword = "needChangePassword";

        /// <summary>
        /// The code application
        /// </summary>
        public const string codeApp = "codeApp";


        /// <summary>
        ///added some fields for postman salary
        /// </summary>
        public const string scopes = "scopes";
        public const string displayName = "displayName";
    }
}
