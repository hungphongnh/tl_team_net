﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace VNP.Salary.Framework.Enums
{
    public class PatternRegex
    {
        public const string TenancyName = "^[a-zA-Z][a-zA-Z0-9_-]{1,}$";
        public const string Email = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        /// <summary>
        /// username is 8-20 characters long
        /// no _ or . at the beginning
        /// no __ or _. or ._ or .. inside
        /// allowed characters
        /// no _ or . at the end
        /// </summary>
        public const string UserName = @"^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-z0-9.@_]+(?<![_.])$";
        /// <summary>
        /// Password matching expression. 
        /// Match all alphanumeric character and predefined wild characters. Password must consists of at least 8 characters and not more than 50 characters.
        /// </summary>
        public const string Password = @"^[a-zA-Z0-9!@#$%^&*()]+$";
        public const string FullName = @"^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
            "ẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
            "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$";

        public static bool IsValid(string value,string pattern)
        {
            if (string.IsNullOrWhiteSpace(value) || string.IsNullOrWhiteSpace(pattern))
            {
                return false;
            }

            var regex = new Regex(pattern);
            return regex.IsMatch(value);
        }
    }
}
