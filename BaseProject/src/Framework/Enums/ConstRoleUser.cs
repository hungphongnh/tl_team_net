﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class ConstRoleUser
    {
        /// <summary>
        /// 
        /// </summary>
        [Category("DEV")]
        [FieldPrivateAttributeAPP(true)]
        [Description("Super Admin")]
        public const string SUPERADMIN = "SUPERADMIN";

        /// <summary>
        /// 
        /// </summary>
        [Description("Admin")]
        public const string ADMIN = "ADMIN";

        /// <summary>
        /// 
        /// </summary>
        [Description("MANAGER")]
        public const string MANAGER = "MANAGER";

        /// <summary>
        /// 
        /// </summary>
        [Description("NORMAL")]
        public const string NORMAL = "NORMAL";

        /// <summary>
        /// Gets the role public.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetRolePublic()
        {
            return new List<string> { ADMIN, MANAGER};
        }
    }
}
