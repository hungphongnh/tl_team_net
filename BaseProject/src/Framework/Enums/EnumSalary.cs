﻿namespace VNP.Salary.Framework.Enums
{
    public class EnumSalary
    {
        public const int DigitSalary = 3;
        /// <summary>
        ///
        /// </summary>
        public const string HSHTCVDVMax = "HSHTCVDV_MAX";
        public const string HSHTCVDVMin = "HSHTCVDV_MIN";

        public const string HSHTQTLMax = "HSHTQTL_MAX";
        public const string HSHTQTLMin = "HSHTQTL_MIN";

        public const string HSHTCVCNMax = "HSHTCVCN_MAX";

        public const string DG_LDQL = "DG_LDQL";


        /// <summary>
        ///KPI/Hệ số chất lượng Max xử lý nghiệp vụ: 1.05
        /// </summary>
        public const string ProfessionalStaff_KPI_QualityCoefficient_Max = "ProfessionalStaff_KPI_QualityCoefficient_Max";

        /// <summary>
        ///KPI/Hệ số chất lượng Min xử lý nghiệp vụ: 0.85
        /// </summary>
        public const string ProfessionalStaff_KPI_QualityCoefficient_Min = "ProfessionalStaff_KPI_QualityCoefficient_Min";


        /// <summary>
        ///KPI/Hệ số chất lượng của cá nhân trong tháng tính lương: 1
        /// </summary>
        public const string ProfessionalStaff_KPI_Quantity_Min = "ProfessionalStaff_KPI_Quantity_Min";

        /// <summary>
        ///Phần trăm trích nộp BHXH %: 10.5%
        /// </summary>
        public const string ProfessionalStaff_PercentageOfInsurancePayment = "ProfessionalStaff_PercentageOfInsurancePayment";

        /// <summary>
        /// 
        /// Mức lương tối thiểu
        /// </summary>
        public const string ProfessionalStaff_SalaryBasicRate = "ProfessionalStaff_SalaryBasicRate";

        /// <summary>
        /// Mức tiền lương tối thiểu vùng I (VNĐ)
        /// </summary>
        public const string ProfessionalStaff_SalaryInsurance = "ProfessionalStaff_SalaryInsurance";
        /// <summary>
        /// Lịch làm việc
        /// </summary>
        public const string ProfessionalStaff_WorkSchedule = "ProfessionalStaff_WorkSchedule";
        /// <summary>
        /// Mức tiền ăn ca
        /// </summary>
        public const string ProfessionalStaff_MidShiftMealSalary = "ProfessionalStaff_MidShiftMealSalary";
        /// <summary>
        /// Mức hỗ trợ đóng BHXH
        /// </summary>
        public const string ProfessionalStaff_SupportOfInsurance = "ProfessionalStaff_SupportOfInsurance";


        /// <summary>
        ///Nhân viên văn hóa xã
        /// </summary>
        public const string ProfessionalStaff_CommuneCultureJobTitle = "ProfessionalStaff_CommuneCultureJobTitle";
        

        public class EnumContractType
        {
            /// <summary>
            /// HĐLĐ không xác định thời hạn
            /// </summary>
            public const string HDKXD = "HĐKXĐ";
            /// <summary>
            /// HĐLĐ từ đủ 12 tháng đến 36 tháng
            /// </summary>
            public const string HD12_36 = "HĐ12-36";
            /// <summary>
            /// HĐLĐ có thời hạn dưới 12 tháng
            /// </summary>
            public const string HDDUOI12 = "HĐDUOI12";
            /// <summary>
            /// Hợp đồng thử việc
            /// </summary>
            public const string HDTV = "HĐTV";
            /// <summary>
            /// Hợp đồng thuê khoán
            /// </summary>
            public const string HDTK = "HĐTK";
            /// <summary>
            /// HĐLĐ có thời hạn 12 tháng
            /// </summary>
            public const string HDTH12 = "HĐTH12";
        }

        public class ContractSpecialObject
        {
            public const int HDTK = 1;
        }
    }
}
