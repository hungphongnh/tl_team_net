﻿
using System.Collections.Generic;

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class EnumRabbitQueueName
    {
        public static List<string> TimekeepingCategoryQueueName()
        {
            return new List<string> { @"jsa.queue.timekeeping_category" };
        }

        public static List<string> ParameterQueueName()
        {
            return new List<string> { @"jsa.queue.parameter" };
        }
        public static List<string> TenantQueueName()
        {
            return new List<string> { @"jsa.queue.Tenant" };
        }
    }
}
