﻿using System.ComponentModel;

namespace VNP.Salary.Framework.Enums
{
    public static class EnumCommon
    {
        public static class EnumExcelRenderType
        {
            /// <summary>
            /// 
            /// </summary>
            [Description("PARMATER")]
            public const int PARAMATER = 1;

            /// <summary>
            /// 
            /// </summary>
            [Description("CODE")]
            public const int CODE = 2;

            /// <summary>
            /// 
            /// </summary>
            [Description("NAME")]
            public const int NAME = 3;
            /// <summary>
            /// 
            /// </summary>
            [Description("INDENTEDTITLE")]
            public const int INDENTED_TITLE = 4;
            /// <summary>
            /// 
            /// </summary>
            [Description("DUPLICATE_CODE")]
            public const int DUPLICATE_CODE = 5;

            /// <summary>
            /// 
            /// </summary>
            [Description("CODETYPE_CODE")]
            public const int CODETYPE_CODE = 6;

            [Description("INSURRANCE_OBJECT")]
            public const int INSURRANCE_OBJECT = 7;

            [Description("SPECIAL_OBJECT")]
            public const int SPECIAL_OBJECT = 8;

            [Description("SALARY_CALCULATION")]
            public const int SALARY_CALCULATION = 9;

            [Description("JOB_TITLE")]
            public const int JOB_TITLE = 10;

            [Description("JOB_POSITION")]
            public const int JOB_POSITION = 11;

            [Description("OBJECT_CATEGORY")]
            public const int OBJECT_CATEGORY = 12;
        }
    }
}
