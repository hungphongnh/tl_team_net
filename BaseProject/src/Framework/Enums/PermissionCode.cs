﻿using System.ComponentModel;

namespace VNP.Salary.Framework.Enums
{
    public class PermissionCode
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("View")]
        public const string View = "View";

        /// <summary>
        /// 
        /// </summary>
        [Description("Add")]
        public const string Add = "Add";

        /// <summary>
        /// 
        /// </summary>
        [Description("Edit")]
        public const string Edit = "Edit";

        /// <summary>
        /// 
        /// </summary>
        [Description("Delete")]
        public const string Delete = "Delete";

        /// <summary>
        /// 
        /// </summary>
        [Description("Import")]
        public const string Import = "Import";

        /// <summary>
        /// 
        /// </summary>
        [Description("Export")]
        public const string Export = "Export";
        /// <summary>
        /// 
        /// </summary>
        [Description("Approve")]
        public const string Approve = "Approve";


    }
}
