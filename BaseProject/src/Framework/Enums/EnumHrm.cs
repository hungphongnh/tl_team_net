﻿using System.ComponentModel;

namespace VNP.Salary.Framework.Enums
{
    public static class EnumHrm
    {
        public const string USERNAME = "hrmapi";

        public const string PASSWORD = "Hrmapi@123";

        public const string CLIENT_ID = "api_test";

        public const string CLIENT_SECRET = "e37d1c7a-e1dd-41d0-a449-b15506550c92";

        public const string GRANT_TYPE = "password";
    }

    public static class EnumLinkHrm
    {
        public const string GETTENANTLEVEL = "nhansu/capdonvi";

        public const string GETALLTENANT = "nhansu/danhsachdonvifull";

        public const string GETTENANT = "nhansu/danhsachdonvi?NgayLayDuLieu={0}";

        public const string GETTENANTNAME = "nhansu/tendonvi";

        public const string GETCONTRACTTYPE = "nhansu/loaihopdong";

        public const string GETEMPLOYEELIST = "nhansu/danhsachnhansu?TuNgay={0}&DenNgay={1}";

        public const string GETWORKINGHISTORY = "nhansu/quatrinhcongtac?TuNgay={0}&DenNgay={1}";

        public const string GETSOCIALINSURANCE = "nhansu/quatrinhluongdongbhxh?TuNgay={0}&DenNgay={1}";

        public const string GETCONTRACT = "nhansu/hopdong?TuNgay={0}&DenNgay={1}";
    }
}
