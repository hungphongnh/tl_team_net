﻿

namespace VNP.Salary.Framework.Enums
{
    /// <summary>
    ///
    /// </summary>
    public class EnumRabbitRoutingKey
    {
        public const string TimekeepingCategoryRoutingKey = @"TimekeepingCategoryChangedIntegrationEvent";

        public const string ParameterRoutingKey = @"ParameterChangedIntegrationEvent";

        public const string TenatnRoutingKey = @"TenantChangedIntegrationEvent";
    }
}
