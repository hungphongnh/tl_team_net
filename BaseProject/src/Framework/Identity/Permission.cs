﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VNP.Salary.Framework.Config;
using VNP.Salary.Framework.Models;
using VNP.Salary.Framework.Response;
using VNP.Salary.Framework.Token;

namespace VNP.Salary.Framework.Identity
{
    public class Permission : Attribute, IAsyncActionFilter

    {
        private string permissionCode;

        public Permission(string permissionCode)
        {
            this.permissionCode = permissionCode;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var endpoint = context.HttpContext.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null) await next();

            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            string controllerName = controllerActionDescriptor?.ControllerName;
            context.HttpContext.Request.Headers.TryGetValue("Authorization", out var authHeader);

            if (string.IsNullOrWhiteSpace(authHeader) || !authHeader.ToString().Trim().Contains(' '))
            {
                context.Result = new ForbidResult();
                return;
            }

            var accessToken = authHeader.ToString().Split(' ')[1];
            string userId = ExtensionHttpContext.GetClaim(accessToken, "userId");

            if (string.IsNullOrWhiteSpace(userId))
            {
                context.Result = new ForbidResult();
                return;
            }

            if (!string.IsNullOrWhiteSpace(permissionCode) && !string.IsNullOrWhiteSpace(controllerName))
            {
                //var authorityConfig = AppCommon.Identity;
                var authorityConfig = context.HttpContext.RequestServices.GetService(typeof(IConfiguration)) as IConfiguration;

                //if (authorityConfig == null || string.IsNullOrWhiteSpace(authorityConfig.Authority))
                if (authorityConfig == null || authorityConfig.GetSection("Identity") == null)
                {
                    context.Result =
                    new BadRequestObjectResult(context.ModelState)
                    {
                        Value = new ObjectResult("Missing some configuration. Please contact with Administrator!")
                    };

                    return;
                }

                authorityConfig.GetSection("Identity").Bind(ConfigCommon.Identity);


                //call api for check permission
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("Authorization", authHeader + "");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var content = JsonConvert.SerializeObject(new PermissionModel() { PageName = controllerName, PermissionCode = permissionCode });

                var bufferContent = new ByteArrayContent(Encoding.UTF8.GetBytes(content));
                bufferContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = await client.PostAsync($"{ConfigCommon.Identity.CheckPermissonUrl}/{userId}", bufferContent);
                if (!response.IsSuccessStatusCode)
                {
                    context.Result = new BadRequestObjectResult(context.ModelState)
                    {
                        Value = new ObjectResult("Missing check permission function. Please contact with Administrator!")
                    };
                    return;
                }

                var isHasPermmsion = bool.Parse(await response.Content.ReadAsStringAsync());

                if (!isHasPermmsion)
                {
                    context.Result = new ForbidResult();
                    return;
                }
            }

            await next();

        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //do somthing after the action executed
        }

        private static void HandlerErrorPermission(ActionExecutingContext context)
        {
        }
    }
}
