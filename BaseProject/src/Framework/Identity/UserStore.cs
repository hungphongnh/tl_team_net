﻿using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using VNP.Salary.Framework.Models;
using static System.Data.CommandType;

namespace VNP.Salary.Framework.Identity
{
    //todo:hannv I need upgrade the method to access database and matching fields in table
    /// <summary>
    /// 
    /// </summary>
    public class UserStore : IUserStore<AspNetUsers>, IUserEmailStore<AspNetUsers>, IUserPhoneNumberStore<AspNetUsers>,
        IUserTwoFactorStore<AspNetUsers>, IUserPasswordStore<AspNetUsers>, IUserRoleStore<AspNetUsers>
    {
        private readonly string _connectionString;
        public UserStore(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private DynamicParameters ConvertObjectToParameter(AspNetUsers entity)
        {
            DynamicParameters parameters = new DynamicParameters();

            foreach (PropertyInfo info in typeof(AspNetUsers).GetProperties())
            {
                string nameOfProperty = info.Name;
                if (nameOfProperty != "Claim" && nameOfProperty != "Password")
                {
                    parameters.Add(nameOfProperty, info.GetValue(entity));
                }
            }
            return parameters;
        }

        public async Task<IdentityResult> CreateAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                //parameters.AddDynamicParams(user);
                parameters = ConvertObjectToParameter(user);
                var result = await connection.ExecuteScalarAsync(typeof(AspNetUsers).Name + "_Insert", param: parameters, commandType: CommandType.StoredProcedure);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                await connection.ExecuteAsync($"DELETE FROM [AspNetUsers] WHERE [Id] = @{nameof(AspNetUsers.Id)}", user);
            }

            return IdentityResult.Success;
        }

        public async Task<AspNetUsers> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                return await connection.QuerySingleOrDefaultAsync<AspNetUsers>($@"SELECT * FROM [AspNetUsers]
                    WHERE [Id] = @{nameof(userId)}", new { userId });
            }
        }

        public async Task<AspNetUsers> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                return await connection.QuerySingleOrDefaultAsync<AspNetUsers>($@"SELECT * FROM [AspNetUsers]
                    WHERE [NormalizedUserName] = @{nameof(normalizedUserName)}", new { normalizedUserName });
            }
        }

        public Task<string> GetNormalizedUserNameAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetUserIdAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(AspNetUsers user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            return Task.FromResult(0);
        }

        public Task SetUserNameAsync(AspNetUsers user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;
            return Task.FromResult(0);
        }

        public async Task<IdentityResult> UpdateAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                //parameters.AddDynamicParams(user);
                parameters = ConvertObjectToParameter(user);
                await connection.ExecuteAsync(typeof(AspNetUsers).Name + "_Update", param: parameters, commandType: StoredProcedure);
            }
            return IdentityResult.Success;
        }

        public Task SetEmailAsync(AspNetUsers user, string email, CancellationToken cancellationToken)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(AspNetUsers user, bool confirmed, CancellationToken cancellationToken)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public async Task<AspNetUsers> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                return await connection.QuerySingleOrDefaultAsync<AspNetUsers>($@"SELECT * FROM [AspNetUsers]
                    WHERE [NormalizedEmail] = @{nameof(normalizedEmail)}", new { normalizedEmail });
            }
        }

        public Task<string> GetNormalizedEmailAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedEmail);
        }

        public Task SetNormalizedEmailAsync(AspNetUsers user, string normalizedEmail, CancellationToken cancellationToken)
        {
            user.NormalizedEmail = normalizedEmail;
            return Task.FromResult(0);
        }

        public Task SetPhoneNumberAsync(AspNetUsers user, string phoneNumber, CancellationToken cancellationToken)
        {
            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public Task<string> GetPhoneNumberAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public Task SetPhoneNumberConfirmedAsync(AspNetUsers user, bool confirmed, CancellationToken cancellationToken)
        {
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task SetTwoFactorEnabledAsync(AspNetUsers user, bool enabled, CancellationToken cancellationToken)
        {
            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task<bool> GetTwoFactorEnabledAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.TwoFactorEnabled);
        }

        public Task SetPasswordHashAsync(AspNetUsers user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public async Task AddToRoleAsync(AspNetUsers user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                var normalizedName = roleName.ToUpper();
                var roleId = await connection.ExecuteScalarAsync<int?>($"SELECT [Id] FROM [AspNetRoles] WHERE [NormalizedName] = @{nameof(normalizedName)}", new { normalizedName });
                if (!roleId.HasValue)
                    roleId = await connection.ExecuteAsync($"INSERT INTO [AspNetRoles]([Name], [NormalizedName]) VALUES(@{nameof(roleName)}, @{nameof(normalizedName)})",
                        new { roleName, normalizedName });

                await connection.ExecuteAsync($"IF NOT EXISTS(SELECT 1 FROM [AspNetUsers] WHERE [Id] = @userId AND [Id] = @{nameof(roleId)}) " +
                    $"INSERT INTO [AspNetUsers]([Id], [Id]) VALUES(@userId, @{nameof(roleId)})",
                    new { userId = user.Id, roleId });
            }
        }

        public async Task RemoveFromRoleAsync(AspNetUsers user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                var roleId = await connection.ExecuteScalarAsync<int?>("SELECT [Id] FROM [AspNetRoles] WHERE [NormalizedName] = @normalizedName", new { normalizedName = roleName.ToUpper() });
                if (!roleId.HasValue)
                    await connection.ExecuteAsync($"DELETE FROM [AspNetUsers] WHERE [Id] = @userId AND [Id] = @{nameof(roleId)}", new { userId = user.Id, roleId });
            }
        }

        public async Task<IList<string>> GetRolesAsync(AspNetUsers user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                var queryResults = await connection.QueryAsync<string>("SELECT r.[Name] FROM [AspNetRoles] r INNER JOIN [AspNetUsers] ur ON ur.[Id] = r.Id " +
                    "WHERE ur.Id = @userId", new { userId = user.Id });

                return queryResults.ToList();
            }
        }

        public async Task<bool> IsInRoleAsync(AspNetUsers user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                var roleId = await connection.ExecuteScalarAsync<int?>("SELECT [Id] FROM [AspNetRoles] WHERE [NormalizedName] = @normalizedName", new { normalizedName = roleName.ToUpper() });
                if (roleId == default(int)) return false;
                var matchingRoles = await connection.ExecuteScalarAsync<int>($"SELECT COUNT(*) FROM [AspNetUsers] WHERE [Id] = @userId AND [Id] = @{nameof(roleId)}",
                    new { userId = user.Id, roleId });

                return matchingRoles > 0;
            }
        }

        public async Task<IList<AspNetUsers>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryResults = await connection.QueryAsync<AspNetUsers>("SELECT u.* FROM [AspNetUsers] u " +
                    "INNER JOIN [AspNetUsers] ur ON ur.[Id] = u.[Id] INNER JOIN [AspNetRoles] r ON r.[Id] = ur.[Id] WHERE r.[NormalizedName] = @normalizedName",
                    new { normalizedName = roleName.ToUpper() });

                return queryResults.ToList();
            }
        }

        #region Add extend method
        public async Task<IdentityResult> DeleteAsync(string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add($"@{nameof(AspNetUsers.Id)}", id);
                await connection.ExecuteAsync($"DELETE FROM [AspNetUsers] WHERE [Id] = @{nameof(AspNetUsers.Id)}", param: parameters);
            }

            return IdentityResult.Success;
        }

        public async Task<string> DeletesAsync(string sWhere, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@sWhere", sWhere);
                var result = await connection.QueryAsync<EntityStringBase>("AspNetUsers_Deletes", param: parameters, commandType: StoredProcedure);
                return result != null ? string.Join(',', result.Select(i => i.Id)) : string.Empty;
            }
        }

        public async Task<(List<AspNetUsersPaging>, long)> Paging(string name, int? usedState, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy, CancellationToken cancellationToken, string listTenantId = "")
        {
            cancellationToken.ThrowIfCancellationRequested();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@name", name);
                parameters.Add("@usedState", usedState);
                parameters.Add("@currentPage", currentPage);
                parameters.Add("@rowsInpage", pageSize);
                parameters.Add("@createdBy", createdBy, DbType.Guid);
                parameters.Add("@tenantId", tenantId, DbType.Guid);
                if(!string.IsNullOrEmpty(listTenantId))
                {
                    parameters.Add("@listTenantId", listTenantId);
                }
                parameters.Add("@totalRows", pageSize, DbType.Int64, ParameterDirection.Output);
                var result = await connection.QueryAsync<AspNetUsersPaging>("AspNetUsers_Paging", parameters, commandType: StoredProcedure);
                long totalRows = parameters.Get<long>("@totalRows");

                return (result.ToList(), totalRows);
            }
        }
        #endregion

        public async Task<string> AddBulk(List<AspNetUsers> entityList, CancellationToken cancellationToken)
        {
            string ids = string.Empty;
            foreach (var entity in entityList)
            {
                var result = await this.CreateAsync(entity, cancellationToken);
                if (result.Succeeded) { ids += entity.Id + ","; }
                ids = ids.TrimEnd(',');
            }
            return ids;
        }

        public async Task<List<AspNetUsersPaging>> Get(string email, Guid? tenantId, CancellationToken cancellationToken)
        {
            var result = await this.Paging(email, null, 1, 1, tenantId, null, cancellationToken);
            return result.Item1;

        }

        public async Task<List<AspNetUsersPaging>> GetAll(CancellationToken cancellationToken)
        {
            var result = await this.Paging(null, null, 1, -1, null, null, cancellationToken);
            return result.Item1;
        }

        public async Task<List<AspNetUsersPaging>> GetByTenant(Guid? tenantId, bool isRecursive, CancellationToken cancellationToken)
        {
            var result = await this.Paging(null, null, 1, -1, tenantId, null, cancellationToken);
            return result.Item1;
        }

        public async Task<bool> IsUserNameExisted(string userName, Guid? tenantId, Guid? id, CancellationToken cancellationToken)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@userName", userName);
                parameters.Add("@tenantId", tenantId);
                parameters.Add("@id", id);
                parameters.Add("@isExisted", false, DbType.Boolean, ParameterDirection.Output);
                var result = await connection.ExecuteScalarAsync( $"{typeof(AspNetUsers).Name}_{nameof(IsUserNameExisted)}", param: parameters, commandType: StoredProcedure);
                return parameters.Get<Boolean>("@isExisted");
            }
        }
        

        public async Task<bool> Updates(string sSet, string sWhere, CancellationToken cancellationToken)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync(cancellationToken);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@sSet", sSet);
                parameters.Add("@sWhere", sWhere);
                var result = await connection.ExecuteScalarAsync(typeof(AspNetUsers).Name + "_Updates", param: parameters, commandType: StoredProcedure);
                
                return (int)result > 0;
            }
        }

        public void Dispose()
        {
            // Nothing to dispose.
        }

    }
}