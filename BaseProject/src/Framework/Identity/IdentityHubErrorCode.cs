﻿using Microsoft.AspNetCore.Identity;

namespace VNP.Salary.Framework.Identity
{
    public class IdentityHubErrorCode : IdentityErrorDescriber
    {

        /// <summary>
        /// 
        /// </summary>
        public override IdentityError DefaultError() { return new IdentityError { Code = nameof(DefaultError), Description = $"Có lỗi, không thể truy cập tài khoản." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordMismatch() { return new IdentityError { Code = nameof(PasswordMismatch), Description = "Mật khẩu không chính xác" }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError InvalidToken() { return new IdentityError { Code = nameof(InvalidToken), Description = "Token không chính xác" }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError LoginAlreadyAssociated() { return new IdentityError { Code = nameof(LoginAlreadyAssociated), Description = "Tài khoản đã được đăng nhập." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError InvalidUserName(string userName) { return new IdentityError { Code = nameof(InvalidUserName), Description = $"Tài khoản '{userName}' không chính xác." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError InvalidEmail(string email) { return new IdentityError { Code = nameof(InvalidEmail), Description = $"Email '{email}' không chính xác." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError DuplicateUserName(string userName) { return new IdentityError { Code = nameof(DuplicateUserName), Description = $"Tài khoản '{userName}' đã tồn tại." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError DuplicateEmail(string email) { return new IdentityError { Code = nameof(DuplicateEmail), Description = $"Email '{email}' đã tồn tại." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError InvalidRoleName(string role) { return new IdentityError { Code = nameof(InvalidRoleName), Description = $"Quyền '{role}' không đúng." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError DuplicateRoleName(string role) { return new IdentityError { Code = nameof(DuplicateRoleName), Description = $"Quyền '{role}' đã tồn tại." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError UserAlreadyHasPassword() { return new IdentityError { Code = nameof(UserAlreadyHasPassword), Description = "Tài khoản đã có mật khẩu." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError UserLockoutNotEnabled() { return new IdentityError { Code = nameof(UserLockoutNotEnabled), Description = "Tài khoản chưa thoát." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError UserAlreadyInRole(string role) { return new IdentityError { Code = nameof(UserAlreadyInRole), Description = $"Tài khoản đã có quyền truy cập." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError UserNotInRole(string role) { return new IdentityError { Code = nameof(UserNotInRole), Description = $"Tài khoản không có quyền truy cập." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordTooShort(int length) { return new IdentityError { Code = nameof(PasswordTooShort), Description = $"Mật khẩu cần có độ dài hơn {length} ký tự." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordRequiresNonAlphanumeric() { return new IdentityError { Code = nameof(PasswordRequiresNonAlphanumeric), Description = "Mật khẩu cần có ký tự đặc biệt." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordRequiresDigit() { return new IdentityError { Code = nameof(PasswordRequiresDigit), Description = "Mật khẩu cần có ký tự số từ '0' đến '9'." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordRequiresLower() { return new IdentityError { Code = nameof(PasswordRequiresLower), Description = "Mật khẩu cần có chữ viêt thường ('a'-'z')." }; }
        /// <summary>
        /// 
        /// </summary>
        public override IdentityError PasswordRequiresUpper() { return new IdentityError { Code = nameof(PasswordRequiresUpper), Description = "Mật khẩu cần có chữ viết hoa ('A'-'Z')." }; }
    }
}