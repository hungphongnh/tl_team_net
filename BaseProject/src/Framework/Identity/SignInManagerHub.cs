﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using VNP.Salary.Framework.Models;

namespace VNP.Salary.Framework.Identity
{
    /// <summary>
    /// TUser = <see cref="AspNetUsers" />
    /// </summary> 
    /// <seealso cref="Microsoft.AspNetCore.Identity.SignInManager{ColUser}" />
    public class SignInManagerHub : SignInManager<AspNetUsers>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignInManagerHub"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="contextAccessor">The context accessor.</param>
        /// <param name="claimsFactory">The claims factory.</param>
        /// <param name="optionsAccessor">The options accessor.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="schemes">The schemes.</param>
        /// <param name="confirmation">The confirmation.</param>
        public SignInManagerHub(UserManagerHub userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<AspNetUsers> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<AspNetUsers>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<AspNetUsers> confirmation) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
        }
    }
}
