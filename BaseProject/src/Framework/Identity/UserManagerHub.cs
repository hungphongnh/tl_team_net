﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Models;

namespace VNP.Salary.Framework.Identity
{
    /// <summary>
    /// TUser = <see cref="AspNetUsers" />
    /// </summary> 
    /// <seealso cref="Microsoft.AspNetCore.Identity.UserManager{ColUser}" />
    public class UserManagerHub : UserManager<AspNetUsers>
    {
        private UserStore myStore;
        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagerHub"/> class.
        /// </summary>
        /// <param name="myStore">My store.</param>
        /// <param name="store">The store.</param>
        /// <param name="optionsAccessor">The options accessor.</param>
        /// <param name="passwordHasher">The password hasher.</param>
        /// <param name="userValidators">The user validators.</param>
        /// <param name="passwordValidators">The password validators.</param>
        /// <param name="keyNormalizer">The key normalizer.</param>
        /// <param name="errors">The errors.</param>
        /// <param name="services">The services.</param>
        /// <param name="logger">The logger.</param>
        public UserManagerHub(UserStore myStore, IUserStore<AspNetUsers> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<AspNetUsers> passwordHasher,
            IEnumerable<IUserValidator<AspNetUsers>> userValidators,
            IEnumerable<IPasswordValidator<AspNetUsers>> passwordValidators,
            ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
            IServiceProvider services, ILogger<UserManager<AspNetUsers>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.myStore = myStore;

        }

        /// <summary>
        /// generate account from fullname
        /// </summary>
        /// <param name="fullName">The full name.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="totalUser">The total user.</param>
        /// <returns></returns>
        public async Task<string> GenerateUserNameAsync(string fullName,  string domain, int totalUser = 1000)
        {
            var listUserName = ExtensionString.GenerateUserName(fullName, domain, totalUser);
            var userList = await myStore.Paging(null, null, 1, -1, null, null, this.CancellationToken);

            if (userList.Item2 > 0)
            {
                var _filter = listUserName.Where(a => userList.Item1.Any(b => b.UserName.Contains(a))).ToList();
                if (_filter != null && _filter.Count > 0)
                {
                    listUserName.RemoveAll(x => _filter.Contains(x));
                }

                var _return = listUserName.FirstOrDefault();
                if (string.IsNullOrWhiteSpace(_return))
                {
                    _return = await GenerateUserNameAsync(fullName, domain, totalUser + 1000);
                }
                return _return;
            }

            return string.Empty;
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override async Task<IdentityResult> CreateAsync(AspNetUsers entity)
        {
            var exists = await this.IsUserNameExisted(entity.UserName,null,null);

            if (!exists)
            {
                var pw = this.PasswordHasher.HashPassword(entity, entity.Password);
                entity.PasswordHash = pw;
                entity.SecurityStamp = String.Concat(Array.ConvertAll(Guid.NewGuid().ToByteArray(), b => b.ToString("X2")));
                entity.ConcurrencyStamp = await this.GenerateConcurrencyStampAsync(entity);
                entity.TwoFactorEnabled = false;
                entity.LockoutEnd = null;
                return await myStore.CreateAsync(entity, this.CancellationToken);
            }
            return IdentityResult.Failed();

        }


        //Write the extension methods here
        //Write the extension methods here
        /// <summary>
        /// Delete item by id string
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IdentityResult> DeleteAsync(string id)
        {
            return await myStore.DeleteAsync(id, this.CancellationToken);
        }

        /// <summary>
        /// Delete items by id list string
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public async Task<string> DeletesAsync(string sWhere)
        {
            return await myStore.DeletesAsync(sWhere, this.CancellationToken);
        }

        public async Task<(List<AspNetUsersPaging>, long)> PagingAsync(string name, int? usedState, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy, string listTenantId = "")
        {
            return await myStore.Paging(name, usedState, currentPage, pageSize, tenantId, createdBy, this.CancellationToken, listTenantId);
        }

        /// <summary>
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public async Task<bool> IsUserNameExisted(string userName, Guid? tenantId, Guid? id)
        {
            return await myStore.IsUserNameExisted(userName,tenantId,id, this.CancellationToken);
        }

    }
}
