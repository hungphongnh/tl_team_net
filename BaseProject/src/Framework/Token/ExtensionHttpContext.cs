﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using VNP.Salary.Framework.Config;

namespace VNP.Salary.Framework.Token
{
    public static class ExtensionHttpContext
    {
        /// <summary>
        /// Gets the token JWT.
        /// </summary>
        /// <param name="Audience">The audience.</param>
        /// <param name="claims">The claims.</param>
        /// <param name="expire_minute">The expire minute.</param>
        /// <returns></returns>
        public static string GetTokenJWT(ConfigAudience Audience, List<Claim> claims, int expire_minute)
        {
            DateTime now = DateTime.UtcNow;
            SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Audience.Secret));
            JwtSecurityToken jwt = new JwtSecurityToken(
                issuer: Audience.Iss,
                audience: Audience.Aud,
                claims: claims,
                notBefore: now.AddDays(-1),
                expires: now.Add(TimeSpan.FromMinutes(expire_minute)),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
            string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public static string GetClaim(string token, string claimType)
        {
            if (string.IsNullOrWhiteSpace(token) || string.IsNullOrWhiteSpace(claimType))
            {
                return string.Empty;
            }

            try
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                return tokenS.Claims.First(claim => claim.Type == claimType).Value;
            }
            catch (System.Exception ex)
            {
                return string.Empty;
            }
        }
        public static string GetClaim2(string token)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var claims = tokenS.Claims.ToList();
                return claims[0].Value;
            }
            catch (System.Exception ex)
            {
                return "";
            }
        }
        /// <summary>
        /// Origins the specified HTTP request.
        /// </summary>
        /// <param name="httpRequest">The HTTP request.</param>
        /// <returns></returns>
        public static string Origin(this HttpRequest httpRequest)
        {
            try
            {
                if (httpRequest.Headers.TryGetValue("origin", out StringValues _return))
                {
                    return _return;
                }
            }
            catch
            {
                // ignored
            }
            return "";
        }

        /// <summary>
        /// Referers the specified HTTP request.
        /// </summary>
        /// <param name="httpRequest">The HTTP request.</param>
        /// <returns></returns>
        public static string Referer(this HttpRequest httpRequest)
        {
            try
            {
                if (httpRequest.Headers.TryGetValue("referer", out StringValues _return))
                {
                    return _return.ToString();
                }
            }
            catch
            {
                // ignored
            }
            return "";
        }

        /// <summary>
        /// Gets the ip client.
        /// </summary>
        /// <param name="RequestClient">The request client.</param>
        /// <returns></returns>
        public static string GetIPClient(this HttpRequest RequestClient)
        {
            RequestClient.Headers.TryGetValue("CF-Connecting-IP", out StringValues remoteIpAddress);
            return remoteIpAddress;
        }
    }
}
