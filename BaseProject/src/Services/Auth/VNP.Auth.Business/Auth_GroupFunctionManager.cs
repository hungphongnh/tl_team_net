﻿using VNP.Auth.Business.Interfaces;
using VNP.Auth.Entities;
using VNP.Auth.Repository.Interfaces;
using VNP.Auth.Business;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace VNP.Core.Business
{
    public class Auth_GroupFunctionManager : Auth_BaseManager<Auth_GroupFunction>, IAuth_GroupFunctionManager
    {
        IAuth_GroupFunctionRepository _groupFunctionRepository;
        public Auth_GroupFunctionManager(IAuth_GroupFunctionRepository groupFunctionRepository) : base(groupFunctionRepository)
        {
            _groupFunctionRepository = groupFunctionRepository;
        }

        public async Task<bool> IsPermission(Guid userId, string functionCode, string permissionCode, Guid? tenantId)
        {
            return await _groupFunctionRepository.IsPermission(userId, functionCode, permissionCode, tenantId);
        }

        public async Task<(List<Auth_GroupFunctionResourcesPaging>, long)> ResourcesPaging(Guid? userId, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy)
        {
            return await _groupFunctionRepository.ResourcesPaging(userId, currentPage, pageSize, tenantId, createdBy);
        }
    }
}
