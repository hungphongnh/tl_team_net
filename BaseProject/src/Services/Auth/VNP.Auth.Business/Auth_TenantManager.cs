﻿using VNP.Auth.Business.Interfaces;
using VNP.Auth.Entities;
using VNP.Auth.Repository.Interfaces;

namespace VNP.Auth.Business
{
    public class Auth_TenantManager : Auth_BaseManager<Auth_Tenant>, IAuth_TenantManager
    {
        IAuth_TenantRepository _groupFunctionRepository;
        public Auth_TenantManager(IAuth_TenantRepository groupFunctionRepository) : base(groupFunctionRepository)
        {
            _groupFunctionRepository = groupFunctionRepository;
        }
    }
}
