﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Newtonsoft.Json;
using VNP.Auth.Business.Interfaces;
using VNP.Auth.Entities.Dto;
using System.Text;
using VNP.Salary.Framework.Response;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace VNP.Auth.Business
{
    public class Auth_APIRepository : IAuth_APIRepository
    {
        private readonly IConfiguration _config;

        public Auth_APIRepository(IConfiguration config)
        {
            _config = config;
        }
        public async Task<CustomApiResponse> GetAPI(string url, string token)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    //httpClient.Timeout = TimeSpan.FromMinutes(1000);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    //using (var cts = new CancellationTokenSource(new TimeSpan(0, 0, 5)))
                    //{
                    //    var result = await httpClient.GetStringAsync(_config.GetSection("APICore").Value + url).ConfigureAwait(false);
                    //    return JsonConvert.DeserializeObject<CustomApiResponse>(result);
                    //}
                    var req = _config.GetSection("APICore").Value + url;
                    var result = await httpClient.GetStringAsync(req);
                    var json = JsonConvert.DeserializeObject<CustomApiResponse>(result);
                    return json;
                }
            }
            catch (Exception ex)
            {
                return new CustomApiResponse();
            }
        }

        public async Task<CustomApiResponse> GetAPI(string url, string token, string content = null)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    //content = Newtonsoft.Json.JsonConvert.SerializeObject(content);
                    StringContent data = new StringContent(content, Encoding.UTF8, "application/json");
                    string uri = _config.GetSection("APICore").Value + url;
                    HttpResponseMessage result = await httpClient.PostAsync(uri, data);

                    if (result == null) return new CustomApiResponse("Error", true);
                    var readStringData = await result.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<CustomApiResponse>(readStringData);
                }
            }
            catch (Exception ex) { }
            return new CustomApiResponse();
        }

        public async Task<ModalRespondUpload> UploadAPI(string url, string token, Auth_ModalSendUpload file)
        {
            try
            {
                var stringPayload = JsonConvert.SerializeObject(file);

                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                string uri = _config.GetSection("APIUploadResourse").Value + url;

                using (var client = new HttpClient())
                {
                    HttpResponseMessage result = await client.PostAsync(uri, httpContent);
                    //var contents = await response.Content.ReadAsStringAsync();

                    //string x = "323";

                    if (result == null) return new ModalRespondUpload();
                    var readStringData = await result.Content.ReadAsStringAsync();

                    var jsonConvertResult = JsonConvert.DeserializeObject<ModalRespondUpload>(readStringData);
                    //g.result.fileResults[0].shortUrl;

                    return jsonConvertResult;
                }
            }
            catch (Exception ex)
            {
                var x = 1;
            }
            return new ModalRespondUpload();
        }
    }
}
