﻿using System.Data;
using VNP.Auth.Entities;
using VNP.Auth.Repository.Interfaces;
using VNP.Salary.Framework.SqlServerDB;

namespace VNP.Auth.Repository
{
    public class Auth_TenantRepository : Auth_BaseRepository<Auth_Tenant>, IAuth_TenantRepository
    {
        private readonly IDbConnection cnn = null;
        public Auth_TenantRepository(SqlServerStorage clientDB)
       : base(clientDB)
        {
            cnn = clientDB.iDbConnection;
        }
    }
}

