﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VNP.Auth.Repository.Interfaces;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Models;
using VNP.Salary.Framework.SqlServerDB;
using static System.Data.CommandType;

namespace VNP.Auth.Repository
{
    public class Auth_BaseRepository<TEntity> : IAuth_BaseRepository<TEntity>, IDisposable where TEntity : class
    {
        private readonly IDbConnection cnn = null;
        protected readonly string TableName = $"Sys{typeof(TEntity).Name.Remove(0, 4)}";

        public Auth_BaseRepository(SqlServerStorage clientDB)
        {
            cnn = clientDB.iDbConnection;
        }
        public void Dispose()
        {
            if (cnn != null)
            {
                cnn.Dispose();
            }
        }
        public async Task<List<TEntity>> GetAll()
        {
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_GetAll", commandType: StoredProcedure);
            return result.ToList();
        }
        public async Task<List<TEntity>> GetByTenant(Guid? tenantId, bool isRecursive)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@tenantId", tenantId, DbType.Guid);
            parameters.Add("@isRecursive", isRecursive, DbType.Boolean);
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_GetByTenant", parameters, commandType: StoredProcedure);
            return result.ToList();
        }

        public async Task<TEntity> Get(object id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Id", id, DbType.String);
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_GetById", parameters, commandType: StoredProcedure);
            return result.FirstOrDefault();
        }

        public async Task<List<TEntity>> Get(string code, Guid? tenantId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Code", code, DbType.String);
            parameters.Add("@TenantId", tenantId, DbType.Guid);
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_GetByCode", parameters, commandType: StoredProcedure);
            return (result).ToList();
        }

        public async Task<Guid> Add(TEntity entity)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.AddDynamicParams(entity);
            parameters.Add("Id", dbType: DbType.Guid, direction: ParameterDirection.ReturnValue);
            var result = await cnn.ExecuteScalarAsync(TableName+ "_Insert", param: parameters, commandType: CommandType.StoredProcedure);
            return (Guid)result;
        }


        public async Task<string> AddBulk(List<TEntity> entityList)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@XMLDOC", ExtensionXml<TEntity>.ToXml(entityList), DbType.String);
            var result = await cnn.QueryAsync<EntityBase>(TableName+ "_Inserts", param: parameters, commandType: StoredProcedure);
            return result != null ? string.Join(',', result.Select(i => i.Id)) : string.Empty;
        }

        public async Task<bool> Update(TEntity entity)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.AddDynamicParams(entity);

            var result = await cnn.ExecuteAsync(TableName+ "_Update", param: parameters, commandType: StoredProcedure);
            return result > 0;
        }

        /// <summary>
        /// Recommendation: Not recommended for API. This code is not safe, maybe SQL Inject
        /// </summary>
        /// <param name="sSet"></param>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public async Task<bool> Updates(string sSet, string sWhere)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@sSet", sSet);
            parameters.Add("@sWhere", sWhere);
            var result = await cnn.ExecuteScalarAsync(TableName+ "_Updates", param: parameters, commandType: StoredProcedure);
            return (int)result > 0;
        }

        public async Task<bool> Delete(string id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            var result = await cnn.ExecuteAsync(TableName+ "_Delete", param: parameters, commandType: StoredProcedure);
            return result > 0;
        }
        /// <summary>
        /// Recommendation: Not recommended for API. This code is not safe, maybe SQL Inject
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public async Task<string> Deletes(string sWhere)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@sWhere", sWhere);
            var result = await cnn.QueryAsync<EntityBase>(TableName+ "_Deletes", param: parameters, commandType: StoredProcedure);
            return result != null ? string.Join(',', result.Select(i => i.Id)) : string.Empty;
        }

        public async Task<(List<TEntity>, long)> Paging(string name, int? usedState, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@name", name);
            parameters.Add("@usedState", usedState);
            parameters.Add("@currentPage", currentPage);
            parameters.Add("@rowsInpage", pageSize);
            parameters.Add("@tenantId", tenantId, DbType.Guid);
            parameters.Add("@createdBy", createdBy, DbType.Guid);
            parameters.Add("@totalRows", pageSize, DbType.Int64, ParameterDirection.Output);

            var result = await cnn.QueryAsync<TEntity>(TableName+ "_Paging", parameters, commandType: StoredProcedure);
            long totalRows = parameters.Get<long>("@totalRows");

            return (result.ToList(), totalRows);
        }

        public async Task<List<TEntity>> GetParent(object id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@parentId", id, DbType.String);
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_GetByParent", parameters, commandType: StoredProcedure);
            return result.ToList();
        }

        public async Task<bool> IsExisted(Guid tenantId, string code)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@tenantId", tenantId, DbType.Guid);
            parameters.Add("@code", code);
            parameters.Add("@isExisted", false, DbType.Boolean, ParameterDirection.Output); ;
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_IsCodeExisted", param: parameters, commandType: StoredProcedure);
            bool isExisted = parameters.Get<Boolean>("@isExisted");
            return isExisted;
        }

        public async Task<List<TEntity>> GetList(string sWhere, string sOrder, int fromRow, int toRow)
        {

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@sWhere", sWhere);
            parameters.Add("@sOrder", sOrder);
            parameters.Add("@fromRow", fromRow);
            parameters.Add("@toRow", toRow);
            var result = await cnn.QueryAsync<TEntity>(TableName+ "_List", parameters, commandType: StoredProcedure);
            return result.ToList();
        }
    }
}

