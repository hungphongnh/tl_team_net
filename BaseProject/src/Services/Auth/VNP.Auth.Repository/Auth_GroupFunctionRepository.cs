﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VNP.Auth.Entities;
using VNP.Auth.Repository.Interfaces;
using VNP.Salary.Framework.SqlServerDB;
using static System.Data.CommandType;

namespace VNP.Auth.Repository
{
    public class Auth_GroupFunctionRepository : Auth_BaseRepository<Auth_GroupFunction>, IAuth_GroupFunctionRepository
    {
        private readonly IDbConnection cnn = null;
        public Auth_GroupFunctionRepository(SqlServerStorage clientDB)
       : base(clientDB)
        {
            cnn = clientDB.iDbConnection;
        }

        public async Task<bool> IsPermission(Guid userId, string functionCode, string permissionCode, Guid? tenantId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@userId", userId, DbType.Guid);
            parameters.Add("@functionCode", functionCode);
            parameters.Add("@permissionCode", permissionCode);
            parameters.Add("@tenantId", tenantId, DbType.Guid);

            var result = await cnn.QueryAsync<bool>($"{this.TableName}_{nameof(IsPermission)}", parameters, commandType: StoredProcedure);

            return result.ToList()[0];
        }

        public async Task<(List<Auth_GroupFunctionResourcesPaging>, long)> ResourcesPaging(Guid? userId,int currentPage, int pageSize, Guid? tenantId, Guid? createdBy)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserId", userId, DbType.Guid);
            parameters.Add("@currentPage", currentPage);
            parameters.Add("@rowsInpage", pageSize);
            parameters.Add("@tenantId", tenantId, DbType.Guid);
            parameters.Add("@createdBy", createdBy, DbType.Guid);
            parameters.Add("@totalRows", pageSize, DbType.Int64, ParameterDirection.Output);

            var result = await cnn.QueryAsync<Auth_GroupFunctionResourcesPaging>($"{this.TableName}_{nameof(ResourcesPaging)}", parameters, commandType: StoredProcedure);
            long totalRows = parameters.Get<long>("@totalRows");

            return (result.ToList(), totalRows);
        }
    }
}

