﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VNP.Auth.Entities;

namespace VNP.Auth.Repository.Interfaces
{
    public interface IAuth_TenantRepository : IAuth_BaseRepository<Auth_Tenant>
    {
    }
}
