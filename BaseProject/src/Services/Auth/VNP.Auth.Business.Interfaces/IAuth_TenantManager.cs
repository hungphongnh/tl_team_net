﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VNP.Auth.Entities;

namespace VNP.Auth.Business.Interfaces
{
    public interface IAuth_TenantManager : IAuth_BaseManager<Auth_Tenant>
    {
    }
}
