﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VNP.Auth.Entities;

namespace VNP.Auth.Business.Interfaces
{
    public interface IAuth_GroupFunctionManager : IAuth_BaseManager<Auth_GroupFunction>
    {
        Task<bool> IsPermission(Guid userId, string functionCode, string permissionCode, Guid? tenantId);
        Task<(List<Auth_GroupFunctionResourcesPaging>, long)> ResourcesPaging(Guid? userId, int currentPage, int pageSize, Guid? tenantId, Guid? createdBy);
    }
}
