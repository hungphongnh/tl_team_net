﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VNP.Auth.Entities.Dto;
using VNP.Salary.Framework.Response;

namespace VNP.Auth.Business.Interfaces
{
    public interface IAuth_APIRepository
    {
        Task<CustomApiResponse> GetAPI(string url, string token);
        Task<CustomApiResponse> GetAPI(string url, string token, string content = null);
        Task<ModalRespondUpload> UploadAPI(string url, string token, Auth_ModalSendUpload content);
    }
}
