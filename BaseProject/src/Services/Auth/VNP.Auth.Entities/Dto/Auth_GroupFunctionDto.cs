﻿using System;
using System.Collections.Generic;
using VNP.Salary.Framework;

namespace VNP.Auth.Entities
{
    public class Auth_GroupFunctionDto : AuditEntity
    {
        public string Id { get; set; }

        public string GroupId { get; set; }

        public string FunctionId { get; set; }

        public string TenantId { get; set; }

        public string PermissionCode { get; set; }

        public string IsAllow { get; set; }

        public string UsedState { get; set; }

        public string Orders { get; set; }
    }

    public class Auth_GroupFunctionResourcesBase
    {
        public Guid Id { get; set; }
        public Guid? TenantId { get; set; }
        public Guid? FunctionId { get; set; }
        public Guid? ParentId { get; set; }
        public string FunctionName { get; set; }
        public string FunctionCode { get; set; }
        public string IconCode { get; set; }
        public string ImagePath { get; set; }
        public int Orders { get; set; }
        public int Level{get; set;}
    }
    public class Auth_GroupFunctionResourcesPaging: Auth_GroupFunctionResourcesBase
    {
        public string ResourceActionsJson { get; set; }
    }

    public class Auth_GroupFunctionResourcesPagingDto : Auth_GroupFunctionResourcesBase
    {
        public List<ResourceActions> ResourceActions { get; set; }
    }

    public class ResourceActions
    {
        public Guid Id { get; set; }
        public string PermissionCode { get; set; }
    }
}
