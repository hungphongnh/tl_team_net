using VNP.Salary.Framework;
namespace VNP.Auth.Entities.Dto
{
    public class IdentityUserDto
    {
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public string EmployeeCode { get; set; }
        public string UserName { get; set; }
    }
}
