﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using VNP.Salary.Framework;
using System.Collections.Generic;

namespace VNP.Auth.Entities.Dto
{
    public class Auth_ModalSendUpload : AuditEntity
    {
        [BindNever]
        public byte[] FileUpload { get; set; }
        public string NameFile { get; set; }
        public string NameTenant { get; set; }
    }
    public class ModalRespondUpload
    {
        public string isError { get; set; }
        public string statusCode { get; set; }
        public string message { get; set; }
        public FileResults result { get; set; }
    }
    public class FileResults
    {
        public List<ModalFileResults> fileResults { get; set; } = new List<ModalFileResults>();
    }

    public class ModalFileResults
    {
        public string name { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string shortUrl { get; set; }
        public string deleteUrl { get; set; }
        public string thumbnailUrl { get; set; }
        public string deleteType { get; set; }
    }
}
