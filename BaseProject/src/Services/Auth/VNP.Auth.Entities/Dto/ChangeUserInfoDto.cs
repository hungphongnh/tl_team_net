using VNP.Salary.Framework;
namespace VNP.Auth.Entities.Dto
{
    public class ChangeUserInfoDto
    {
        public string Data { get; set; }
    }
}
