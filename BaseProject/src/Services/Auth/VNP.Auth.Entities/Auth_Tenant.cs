﻿using System;
using VNP.Salary.Framework;

namespace VNP.Auth.Entities
{
    public class Auth_Tenant : AuditEntity
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public Guid ParentId { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string ParentCode { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string TenantType { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string AbbreviationName { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string InsuranceRegionId { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string UsedState { get; set; }
        /// <summary>
        /// Gets or sets value  
        /// </summary>
        public string Orders { get; set; }
    }
}
