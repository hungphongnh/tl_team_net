﻿using System;
using VNP.Salary.Framework;

namespace VNP.Auth.Entities
{
    public class Auth_GroupFunction : AuditEntity
    {
        public Guid Id { get; set; }

        public Guid? GroupId { get; set; }

        public Guid? FunctionId { get; set; }

        public Guid? TenantId { get; set; }

        public string PermissionCode { get; set; }

        public bool? IsAllow { get; set; }

        public int? UsedState { get; set; }

        public int? Orders { get; set; }

    }
}
