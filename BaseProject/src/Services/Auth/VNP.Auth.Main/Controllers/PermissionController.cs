﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VNP.Auth.Business.Interfaces;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Models;

namespace VNP.Auth.Main.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class PermissionController : ControllerBase
    {
        IAuth_GroupFunctionManager groupFunctionManager;

        public PermissionController(IAuth_GroupFunctionManager _groupFunctionManager)
        {
            groupFunctionManager = _groupFunctionManager;
        }
        /// <summary>
        /// Check user has permission on the page
        /// </summary>
        /// <param name="id">UserId</param>
        /// <param name="model">permission model</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task<bool> Post(string id, [FromBody]PermissionModel model)
        {
            if(string.IsNullOrWhiteSpace(id) || model == null || string.IsNullOrWhiteSpace(model.PageName) || string.IsNullOrWhiteSpace(model.PermissionCode))
            {
                return false;
            }

            Guid? userId = ExtensionGuid.ToGuidOrNull(id);

            if(userId == null || userId == Guid.Empty) { return false; }

            return await groupFunctionManager.IsPermission(userId.Value, model.PageName, model.PermissionCode, null);
        }
    }
}
