﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using VNP.Auth.Business.Interfaces;
using VNP.Auth.Entities;
using VNP.Auth.Main.Interface;
using VNP.Auth.Main.Models;
using VNP.Salary.Framework.Enums;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Identity;
using VNP.Salary.Framework.Models;
using VNP.Salary.Framework.Response;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace VNP.Auth.Main.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class AccountController : ControllerBase
    {
        //#region Attribute
        private IMapper _mapper;
        private SignInManagerHub _signInManager;
        private UserManagerHub _userManager;
        private IAccountHub _accountHub;
        private IAuth_GroupFunctionManager _groupFunctionManager;
        private IAuth_TenantManager _tenantManager;

        public AccountController(UserManagerHub userManager, SignInManagerHub signInManager, IAccountHub accountHub, IMapper mapper, IAuth_GroupFunctionManager groupFunctionManager,
            IAuth_TenantManager tenantManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _accountHub = accountHub;
            _groupFunctionManager = groupFunctionManager;
            _mapper = mapper;
            _tenantManager = tenantManager;
        }

        private async Task<string> getFunctionAndAction(Guid? userId)
        {
            var result = string.Empty;

            if (userId.HasValue)
            {
                var actionList = await _groupFunctionManager.ResourcesPaging(userId, 1, -1, null, null);
                if (actionList.Item1 != null && actionList.Item1.Count > 0)
                {
                    foreach (var function in actionList.Item1)
                    {
                        var jsonResourceActions = JsonConvert.DeserializeObject<List<ResourceActions>>(function.ResourceActionsJson);
                        if (!string.IsNullOrWhiteSpace(function.FunctionCode) && jsonResourceActions != null && jsonResourceActions.Count > 0)
                        {
                            foreach (var action in jsonResourceActions) result += $"{function.FunctionCode}_{action.PermissionCode},";
                        }
                    }
                }
            }
            return result.TrimEnd(',');


        }

        /// <summary>
        /// Login native function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<CustomApiResponse> loginNative(FormPostLoginNative model)
        {
            CustomApiResponse customApiResponse = new CustomApiResponse();
            SignInResult result = await _signInManager.PasswordSignInAsync(model.userName, model.password, model.rememberMe, true);

            if (result.Succeeded)
            {
                AspNetUsers user = await _userManager.FindByNameAsync(model.userName);
                if (user != null)
                {
                    if (user.UsedState != EnumUsedState.Active)
                    {
                        customApiResponse.IsError = true;
                        switch (user.UsedState)
                        {
                            case EnumUsedState.DeActive:
                                customApiResponse.Message = "Tài khoản chưa được kích hoạt";
                                break;
                            case EnumUsedState.Deleted:
                                customApiResponse.Message = $"Tài khoản '{model.userName}' không tồn tại";
                                break;
                        }

                        return customApiResponse;
                    }

                    await _userManager.UpdateAsync(user);
                    int _expire_minute = ExtensionCommon.GetExpireMinute(model.rememberMe);
                    //if (user.isVerified == false) // tk chưa đc xác nhận sđt
                    //{
                    //    _expire_minute = 50;
                    //}

                    Guid? userId = ExtensionGuid.ToGuid(user.Id);
                    //if (model.isPostmanAuthorize.HasValue && model.isPostmanAuthorize.Value) userId = null;

                    var tenantModel = _tenantManager.Get(user.TenantId + "");
                    var getActionString = getFunctionAndAction(userId);
                    await Task.WhenAll(tenantModel, getActionString);
                    var packages = new List<string>();
                    //packages.Add(await getActionString);


                    customApiResponse.Result = _accountHub.CreateTokenJwt(user, _expire_minute, await tenantModel, packages);
                    customApiResponse.Message = "Đăng nhập thành công!";
                    //if (!string.IsNullOrWhiteSpace(_return.access_token))
                    {
                        //Tạo Notfy login thành công
                        //BackgroundJob.Enqueue<BackgroundJobOauth>(obj => obj.CreateNotfyLoginSuccess(user.userId, user.userName, DateTime.Now.ToStringVN()));
                        //await accountHub.CreateUserActiveLogin(user.id, user.userName, user.GetCodeApp(), user.SchoolCurrent, this.HttpContext);
                    }
                }
                else
                {
                    customApiResponse.Message = $"Tài khoản '{model.userName}' không tồn tại";
                }
            }
            else
            {
                AspNetUsers user = await _userManager.FindByNameAsync(model.userName);
                customApiResponse.Message = _accountHub.GetMessageSignIn(model.userName, result, user);
                customApiResponse.IsError = !result.Succeeded;
            }
            return customApiResponse;
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<CustomApiResponse>> Login([FromBody] FormPostLogin model)
        {
            CustomApiResponse _return = new CustomApiResponse();

            if (string.IsNullOrWhiteSpace(model.userName) || string.IsNullOrWhiteSpace(model.password))
            {
                return new CustomApiResponse("Tài khoản hoặc mật khẩu không được trống!", true);
            }

            model.socialType = model.socialType?.ToUpper();
            switch (model.socialType)
            {
                #region ACCOUNTKIT Login Option
                //case ConstSocialType.ACCOUNTKIT:
                //    _return = await loginPhoneNumber(new FormPostLoginPhone
                //    {
                //        phoneNumber = model.userName,
                //        password = model.password,
                //        rememberMe = model.rememberMe
                //    });
                //    break;
                #endregion
                default:
                    _return = await loginNative(
                        new FormPostLoginNative
                        {
                            userName = model.userName,
                            password = model.password,
                            rememberMe = false
                        });
                    break;
            }

            #region Add Package for tenant
            //if (!string.IsNullOrWhiteSpace(_return.access_token))
            //{
            //    await _da_account.SetPackageDefault(_return.userid);
            //}
            #endregion

            return _return;
        }
        /// <summary>
        /// Get user info
        /// </summary>
        /// <param name="id">UserId</param>
        /// <returns></returns>
        [HttpGet("UserInfo")]
        [Authorize]
        public async Task<ActionResult<CustomApiResponse>> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return new CustomApiResponse("Dữ liệu trống!");
            }

            ResponseUserInfo _return = new ResponseUserInfo();
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                _return = new ResponseUserInfo
                {
                    TenantId = user.TenantId + "",
                    TenantType = user.TenantId + "",
                    UserId = user.Id + "",
                    UserName = user.UserName,
                    DisplayName = user.FullName,
                    Email = user.Email,
                    Gender = "",
                    PhoneNumber = "",
                    Birthday = "",
                    Avatar = user.Avatar,
                    IsVerified = user.UsedState == EnumUsedState.Active
                };
            }
            return new CustomApiResponse(_return);
        }

        [HttpGet("ResourceActions/{id}")]
        [Authorize]
        public async Task<CustomApiResponse> ResourceActions(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return new CustomApiResponse("Dữ liệu trống!");
            }
            Guid? userId = ExtensionGuid.ToGuidOrNull(id);
            if (!userId.HasValue || userId.Value == Guid.Empty)
            {
                return new CustomApiResponse("Dữ liệu không hợp lệ!");
            }

            var result = await _groupFunctionManager.ResourcesPaging(userId.Value, 1, -1, null, null);
            var resultData = _mapper.Map<List<Auth_GroupFunctionResourcesPagingDto>>(result.Item1);
            return new CustomApiResponse(resultData);
        }
    }
}
