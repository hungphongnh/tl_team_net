﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VNP.Auth.Business.Interfaces;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Identity;
using VNP.Salary.Framework.Token;
using VNP.Salary.Framework.Response;
using AutoMapper;
using VNP.Auth.Entities.Dto;
using Microsoft.AspNetCore.Identity;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace VNP.Auth.Main.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class UserController : ControllerBase
    {
        IMapper _mapper;
        UserManagerHub _userManager;
        SignInManagerHub _signInManager;
        IAuth_APIRepository _apiRepository;
        IConfiguration _config;

        public UserController(UserManagerHub userManager, SignInManagerHub signInManager, IMapper mapper, IAuth_APIRepository apiRepository, IConfiguration config)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _apiRepository = apiRepository;
            _config = config;
        }
        [HttpGet]
        public async Task<CustomApiResponse> GetUserInfo()
        {
            Request.Headers.TryGetValue("Authorization", out var token);
            token = token.ToString().Replace("Bearer ", "");
            string id = ExtensionHttpContext.GetClaim(token, "userId");
            var user = await _userManager.FindByIdAsync(id);
            var userDto = _mapper.Map<IdentityUserDto>(user);
            return new CustomApiResponse() { IsError = false, Result = userDto };
        }
        [HttpPost]
        public async Task<CustomApiResponse> ChangePassword(ChangeUserInfoDto password)
        {
            Request.Headers.TryGetValue("Authorization", out var token);
            token = token.ToString().Replace("Bearer ", "");
            string id = ExtensionHttpContext.GetClaim(token, "userId");
            var user = await _userManager.FindByIdAsync(id);
            var rsToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            IdentityResult rs = await _userManager.ResetPasswordAsync(user, rsToken, password.Data);
            return new CustomApiResponse() { IsError = false, Result = rs };
        }
        [HttpPatch]
        public async Task<CustomApiResponse> ChangeAvatar(ChangeUserInfoDto avatar)
        {
            Request.Headers.TryGetValue("Authorization", out var token);
            token = token.ToString().Replace("Bearer ", "");
            string id = ExtensionHttpContext.GetClaim(token, "userId");
            var user = await _userManager.FindByIdAsync(id);
            var base64 = avatar.Data.Split(',')[1];
            byte[] bytes = Convert.FromBase64String(base64);
            Auth_ModalSendUpload upload = new Auth_ModalSendUpload()
            {
                FileUpload = bytes,
                NameFile = user.UserName + ".png",
                NameTenant = "avatar"
            };
            ModalRespondUpload responseUpload = await _apiRepository.UploadAPI("FileUpload/UploadImage", token, upload);
            var shortLinkReport = responseUpload.result.fileResults[0];
            var link = ($"{_config.GetSection("APIResourse").Value}{shortLinkReport.shortUrl}").Replace("\\", "/");
            user.Avatar = link;
            var result = await _userManager.UpdateAsync(user);
            return new CustomApiResponse() { IsError = false, Result = result };
        }
    }
}
