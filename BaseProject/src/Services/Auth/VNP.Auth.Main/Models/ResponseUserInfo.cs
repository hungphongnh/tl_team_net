﻿namespace VNP.Auth.Main.Models
{
    public class ResponseUserInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string TenantId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string TenantType { set; get; }

        public string UserId { set; get; }
        /// <summary>
        ///     User name
        /// </summary>
        public string UserName { set; get; }
        /// <summary>
        ///     Email email
        /// </summary>
        public string Email { set; get; }
        /// <summary>
        ///     Email email
        /// </summary>
        public string PhoneNumber { set; get; }

        /// <summary>
        ///     
        /// </summary>
        public string DisplayName { set; get; }
        /// <summary>
        /// Gets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public string Gender { get; internal set; }
        /// <summary>
        /// Gets the Birthday.
        /// </summary>
        /// <value>
        /// The Birthday.
        /// </value>
        public string Birthday { get; internal set; }
        
        /// <summary>
        ///     Url avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 
        /// <para>True : authenticated</para>
        /// <para>False : Not authenticate</para>
        /// </summary>
        public bool IsVerified { get; set; } = true;
    }

}
