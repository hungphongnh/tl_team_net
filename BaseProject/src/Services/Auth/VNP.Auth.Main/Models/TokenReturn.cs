﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNP.Auth.Main.Models
{
    public class TokenReturn
    {
        /// <summary>
        /// token
        /// </summary>
        /// <value></value>
        public string access_token { set; get; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public int expires_in { set; get; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <para> 403: Phone number does not match </para>
        /// <para> 500: Lack of credentials </para>
        /// <para> 302: Authentication error </para>
        /// <para> 404: Account creation error </para>
        /// <para> 200: Authentication succeeded </para>
        //public int status { set; get; } = 302;
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string message { set; get; }

        /// <summary>
        ///     User name
        /// </summary>
        public string userName { set; get; }

        /// <summary>
        ///     
        /// </summary>
        public string displayName { set; get; }

        /// <summary>
        ///     ID user
        /// </summary>
        public string userid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string tenantId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string tenantName { set; get; }
        /// <summary>
        /// /
        /// </summary>
        public string tenantType { set; get; }

        /// <summary>
        ///     Url avatar
        /// </summary>
        public string avatar { get; set; }

        /// <summary>
        /// 
        /// <para>True : authenticated</para>
        /// <para>False : Not authenticate</para>
        /// </summary>
        public bool isVerified { get; set; } = true;
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        //public string action { set; get; } = "login";
    }
}
