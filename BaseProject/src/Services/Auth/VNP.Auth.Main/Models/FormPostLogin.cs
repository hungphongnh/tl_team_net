﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VNP.Auth.Main.Models
{
    public class FormPostLogin
    {
        /// <summary>
        ///     Gets or sets the name of the user.
        /// </summary>
        /// <value>
        ///     The name of the user.
        /// </value>
        public string userName { set; get; }

        /// <summary>
        ///     Mật khẩu
        /// </summary>
        public string password { get; set; }

        /// <summary>
        ///     
        /// <para>1. True: Remember to log in 2 days</para>
        /// <para>2. False : Remember to login 6 hours</para>
        /// </summary>
        public bool rememberMe { get; set; }

        /// <summary>
        ///     Id social (Google, facebook, zalo)
        /// </summary>
        public string socialId { get; set; }

        /// <summary>
        /// <para> 1. Email: login with userName and password </para>
        /// <para> 2. GOOGLE: login with google </para>
        /// <para> 3. PHONEOTP: login with sdt using otp </para>
        /// <para> 4. ACCOUNTKIT: login with facebook account kit </para>
        /// </summary>
        public string socialType { get; set; }

        /// <summary>
        ///     token social
        /// </summary>
        public string socialToken { get; set; }
    }

    /// <summary>
    /// </summary>
    public class FormPostLoginNative
    {
        /// <summary>
        ///     Gets or sets the name of the user.
        /// </summary>
        /// <value>
        ///     The name of the user.
        /// </value>
        public string userName { set; get; }

        /// <summary>
        ///     The password of the user
        /// </summary>
        public string password { get; set; }

        /// <summary>
        ///     The remember password
        /// <para>1. True: Remember to log in 2 days</para>
        /// <para>2. False : Remember to login 6 hours</para>
        /// </summary>
        public bool rememberMe { get; set; }

        public bool? isPostmanAuthorize { get; set; } = true;
    }

    public class FormPostLoginPhone
    {
        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for phoneNumber
        [Required]
        public string phoneNumber { get; set; }
        /// <summary>
        ///     Gets or sets the password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        [Required]
        [MinLength(4)]
        public string password { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [remember me].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [remember me]; otherwise, <c>false</c>.
        /// </value>
        public bool rememberMe { get; set; } = true;
    }
}
