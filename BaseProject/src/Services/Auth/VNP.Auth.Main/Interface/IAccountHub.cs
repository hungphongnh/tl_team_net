﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using VNP.Auth.Entities;
using VNP.Auth.Main.Models;
using VNP.Salary.Framework.Models;

namespace VNP.Auth.Main.Interface
{
    public interface IAccountHub
    {
        TokenReturn CreateTokenJwt(AspNetUsers user, int expire_minute,Auth_Tenant tenant, List<string> packages = null);
        string GetMessageSignIn(string userName, SignInResult result, AspNetUsers user);
    }
}
