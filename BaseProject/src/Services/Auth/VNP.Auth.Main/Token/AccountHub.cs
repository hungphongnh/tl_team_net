﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using VNP.Auth.Entities;
using VNP.Auth.Main.Interface;
using VNP.Auth.Main.Models;
using VNP.Salary.Framework.Config;
using VNP.Salary.Framework.Enums;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Models;
using VNP.Salary.Framework.Token;

namespace VNP.Auth.Main.Token
{
    public class AccountHub: IAccountHub
    {
        private readonly IOptions<ConfigAudience> _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountHub"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public AccountHub(IOptions<ConfigAudience> configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="expire_minute"></param>
        /// <param name="packages"></param>
        /// <returns></returns>
        public TokenReturn CreateTokenJwt(AspNetUsers user, int expire_minute,Auth_Tenant tenant, List<string> packages = null)
        {
            if (user != null)
            {
                var _return = new TokenReturn
                {
                    expires_in = expire_minute * 60,
                    userName = user.UserName,
                    displayName = user.FullName,
                    userid = user.Id,
                    tenantId = tenant.Id+"",
                    tenantName = tenant.Name,
                    tenantType = tenant.TenantType,
                    avatar = user.Avatar,
                    isVerified = user.UsedState==1,
                    message = "Success",
                };
                var listClaim = user.Claim();
                listClaim.Add(new Claim(EnumClaimType.tenantType, tenant.TenantType));
                listClaim.Add(new Claim(EnumClaimType.displayName, user.FullName));
                if(packages != null)
                    listClaim.Add(new Claim(EnumClaimType.scopes, string.Join(",", packages)));

                _return.access_token = ExtensionHttpContext.GetTokenJWT(_configuration.Value, listClaim, expire_minute);


                return _return;
            }
            return null;
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="result">The result.</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GetMessageSignIn(string userName, SignInResult result, AspNetUsers user)
        {
            string message = "";
            if (result.Succeeded)
            {
                message = "Đăng nhập thành công";
            }
            else if (result.RequiresTwoFactor)
            {
                message = "Yêu cầu xác thực 2 lớp";
            }
            else if (result.IsLockedOut)
            {
                if (user != null && user.LockoutEnd != null)
                {
                    //TimeSpan d = user.lockoutEndDate.Instant.ToLocalTime() - DateTime.Now;
                    TimeSpan d = user.LockoutEnd.Value.ToLocalTime() - DateTime.Now;
                    message = $"Tài khoản tạm khóa. Thử lại sau {d.TotalMinutes.ToInt() + 1} phút";
                }
                else
                {
                    message = "Tài khoản tạm khóa";
                }
            }
            else
            {
                if (user != null)
                {
                    if (user.UsedState == EnumUsedState.DeActive)
                    {
                        message = "Tài khoản chưa được kích hoạt";
                    }else if(user.UsedState == EnumUsedState.Deleted) {
                        message = $"Tài khoản '{userName}' không tồn tại";
                    }
                    else
                    {
                        message = "Mật khẩu không chính xác";
                    }
                }
                else
                {
                    message = $"Tài khoản '{userName}' không tồn tại";
                }
            }
            return message;
        }

    }
}