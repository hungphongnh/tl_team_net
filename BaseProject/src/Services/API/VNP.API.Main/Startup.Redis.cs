﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using VNP.Salary.Framework.Config;
using VNP.Salary.Framework.Redis;

namespace VNP.API.Main
{
    public partial class Startup
    {
        /// <summary>
        /// Configures the JWT authentication service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="Configuration">The configuration.</param>
        /// <returns></returns>
        public void AddRedis(IServiceCollection services, IConfiguration configuration)
        {
            //services.AddStackExchangeRedisCache(options =>
            //{
            //    options.Configuration = "127.0.0.1:6379";
            //    options.InstanceName = "InstanceMaster";
            //});

            //services.AddStackExchangeRedisCache(options =>
            //{
            //    options.Configuration = "127.0.0.1:6380";
            //    options.InstanceName = "InstanceSlave";
            //});

            //services.Configure<RedisCacheOptions1>(options =>
            //{
            //    options.Configuration = "127.0.0.1:6379";
            //    options.InstanceName = "InstanceSample";
            //});s
            //services.Configure<RedisCacheOptions2>(options =>
            //{
            //    options.Configuration = "127.0.0.1:6379";
            //    options.InstanceName = "InstanceSample";
            //});

            services.Configure<RedisCacheOptionsMaster>(configuration.GetSection("RedisConfigMaster"));
            services.Configure<RedisCacheOptionsSlave>(configuration.GetSection("RedisConfigSlave"));
            services.Add(ServiceDescriptor.Singleton<IDistributedCacheMaster, RedisCacheMaster>());
            services.Add(ServiceDescriptor.Singleton<IDistributedCacheSlave, RedisCacheSlave>());

        }

    }
}
