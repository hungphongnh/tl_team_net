using Autofac;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StackExchange.Redis;
using Microsoft.Extensions.Logging;
using VNP.API.Main.Middlewares;
using VNP.Salary.Framework.Config;
using Microsoft.AspNetCore.Http;
using VNP.Salary.Framework.Signal;
using VNP.Salary.Framework.BackgroundTask;
using System.Collections.Generic;

namespace VNP.API.Main
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private string _policyNameAllow = "MyAllowSpecificOrigins";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add config header allow to connect from client app
            //services.AddCors();
            services.AddCors(options =>
            {
                options.AddPolicy(
                    name: _policyNameAllow,
                    builder =>
                    {
                        builder
                            .WithOrigins(
                                "https://salary.vnpost.vn",
                                "http://salary.vnpost.vn",
                                "https://localhost:44313",
                                "http://localhost:8200",
                                "http://localhost:44386",
                                "https://localhost:44386",
                                "http://elearning-uat.vnpost.vn",
                                "https://elearning-uat.vnpost.vn",
                                "http://salary-uat.vnpost.vn",
                                "https://salary-uat.vnpost.vn",
                                "https://api-salary.vnpost.vn",
                                "http://api-salary.vnpost.vn",
                                "https://luong.vnpost.vn",
                                "http://luong.vnpost.vn",
                                "https://document-salary.vnpost.vn",
                                "http://document-salary.vnpost.vn",
                                "https://api-salary.vnpost.vn/ProfessionalStaff",
                                "http://api-salary.vnpost.vn/ProfessionalStaff",
                                "https://luong.vnpost.vn/ProfessionalStaff",
                                "http://luong.vnpost.vn/ProfessionalStaff",
                                "https://document-salary.vnpost.vn",
                                "http://document-salary.vnpost.vn"
                                )
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                        builder.AllowCredentials();
                    });
            });

            //Config API version
            services.AddApiVersioning(/*o => o.ApiVersionReader = new HeaderApiVersionReader("api-version")*/);

            //Add authention with Jwt
            this.AddAuthJwt(services, this.Configuration);

            services.AddControllers().AddNewtonsoftJson();

            //Add Redis cache
            this.AddRedis(services, this.Configuration);

            //Add Identity User 
            services.AddIdentityHub();
            services.Configure<ConfigAudience>(Configuration.GetSection("Audience"));

            //Configuration.GetSection("Identity").Bind(AppCommon.Identity);

            this.AddSqlServerStorage(services, this.Configuration);

            //Add DependencyInjection class
            this.AddDependencyInjection(services, this.Configuration);

            //Add startup partial class configure services 
            this.ConfigureSwagger(services);

            //Config Automap
            var autoFacConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            autoFacConfig.AssertConfigurationIsValid();
            services.AddSingleton(autoFacConfig.CreateMapper());
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddSignalR();
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureCustomExceptionMiddleware();

            #region Use startup partial class configure services
            UseSwaggerUI(app, env);
            #endregion

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(_policyNameAllow);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapGet("/echo",
                //context => context.Response.WriteAsync("echo"))
                //.RequireCors(_policyNameAllow);

                //endpoints.MapControllers()
                //         .RequireCors(_policyNameAllow);
                endpoints.MapHub<HandlerHub>("/hub");
            });
        }
    }
}
