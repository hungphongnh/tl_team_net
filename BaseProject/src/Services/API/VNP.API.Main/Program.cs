using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.IO;
using VNP.API.Main.Extension;
using VNP.Salary.Framework.Extension;

namespace VNP.API.Main
{
    public class Program
    {
        //public static void Main(string[] args)
        //{
        //    Log.Logger = new LoggerConfiguration()
        //    .MinimumLevel.Debug()
        //    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
        //    .MinimumLevel.Override("System", LogEventLevel.Error)
        //    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        //    .MinimumLevel.Override("System", LogEventLevel.Information)
        //    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
        //    .MinimumLevel.Override("System", LogEventLevel.Warning)

        //    .Enrich.FromLogContext()
        //    .WriteTo.File(Environment.CurrentDirectory + @"\Logs\" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt",
        //        fileSizeLimitBytes: 1_000_000,
        //        rollOnFileSizeLimit: true,
        //        shared: true,
        //        flushToDiskInterval: TimeSpan.FromSeconds(1)
        //        , outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
        //    .CreateLogger();

        //    CreateWebHostBuilder(args).UseAutofac().Build().Run();

        //}


        //public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //    //.UseServiceProviderFactory(new AutofacServiceProviderFactory())

        //    .UseSerilog()
        //    .ConfigureAppConfiguration(
        //        configHost =>
        //        {
        //            configHost.SetBasePath(Directory.GetCurrentDirectory());
        //            configHost.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
        //            configHost.AddJsonFile($"appsettings.{ ExtensionCommon.GetEnvironmentName()}.json", optional: false, reloadOnChange: true);
        //            configHost.AddCommandLine(args);

        //        }
        //        ).UseStartup<Startup>();

        public static void Main(string[] args)
        {


            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
            .MinimumLevel.Override("System", LogEventLevel.Error)
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .MinimumLevel.Override("System", LogEventLevel.Information)
            .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            .MinimumLevel.Override("System", LogEventLevel.Warning)

            .Enrich.FromLogContext()
            .WriteTo.File(Environment.CurrentDirectory + @"\Logs\" + DateTime.Now.ToString("dd_MM_yyyy") + ".txt",
                fileSizeLimitBytes: 1_000_000,
                rollOnFileSizeLimit: true,
                shared: true,
                flushToDiskInterval: TimeSpan.FromSeconds(1)
                , outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
            .CreateLogger();

            //CreateWebHostBuilder(args).UseAutofac().Build().Run();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>

        //    WebHost.CreateDefaultBuilder(args)
        //    //.UseServiceProviderFactory(new AutofacServiceProviderFactory())

        //    .UseSerilog()
        //    .ConfigureAppConfiguration(
        //        configHost =>
        //        {
        //            configHost.SetBasePath(Directory.GetCurrentDirectory());
        //            configHost.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
        //            configHost.AddJsonFile($"appsettings.{ ExtensionCommon.GetEnvironmentName()}.json", optional: false, reloadOnChange: true);
        //            configHost.AddCommandLine(args);

        //        }
        //        ).UseStartup<Startup>();


        Host.CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
            .UseSerilog()
                .ConfigureAppConfiguration(
                    configHost =>
                    {
                        configHost.SetBasePath(Directory.GetCurrentDirectory());
                        configHost.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                        configHost.AddJsonFile($"appsettings.{ ExtensionCommon.GetEnvironmentName()}.json", optional: false, reloadOnChange: true);
                        configHost.AddCommandLine(args);

                    }
                    )
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

    }



}





