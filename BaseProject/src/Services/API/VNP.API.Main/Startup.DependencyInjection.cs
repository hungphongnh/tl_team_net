﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VNP.Auth.Business;
using VNP.Auth.Business.Interfaces;
using VNP.Auth.Main.Interface;
using VNP.Auth.Main.Token;
using VNP.Auth.Repository;
using VNP.Auth.Repository.Interfaces;
using VNP.Core.Business;
using VNP.Salary.Framework.Interface;
using VNP.Salary.Framework.Logger;

namespace VNP.API.Main
{
    public partial class Startup
    {
        private void AddCoreDependencyInjection(IServiceCollection services, IConfiguration configuration)
        {
            //DI Log
            services.AddSingleton<ILoggerManager, LoggerManager>();

            //DI Model into startup
            services.AddSingleton<IAccountHub, AccountHub>();
        }

        private void AddAuthDependencyInjection(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IAuth_GroupFunctionManager, Auth_GroupFunctionManager>();
            services.AddScoped<IAuth_GroupFunctionRepository, Auth_GroupFunctionRepository>();
            services.AddScoped<IAuth_TenantManager, Auth_TenantManager>();
            services.AddScoped<IAuth_TenantRepository, Auth_TenantRepository>();
            services.AddScoped<IAuth_APIRepository, Auth_APIRepository>();
        }
        /// <summary>
        /// Adds AddDependencyInjection
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="Configuration">The configuration.</param>
        public void AddDependencyInjection(IServiceCollection services, IConfiguration configuration)
        {
            this.AddCoreDependencyInjection(services, configuration);
            this.AddAuthDependencyInjection(services, configuration);
        }
    }
}
