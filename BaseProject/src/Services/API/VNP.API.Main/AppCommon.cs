﻿using VNP.Salary.Framework.Config;

namespace VNP.API.Main
{
    public class AppCommon
    {
        /// <summary>
        /// The configuration audience
        /// </summary>
        public static ConfigAudience Audience = new ConfigAudience();
        //public static Identity Identity = new Identity();
    }
}
