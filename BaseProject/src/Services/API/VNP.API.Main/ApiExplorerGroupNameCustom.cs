﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;

namespace VNP.API.Main
{
    public class ApiExplorerGroupNameCustom : IControllerModelConvention
    {
        /// <summary>
        /// 
        /// </summary>
        public const string API_CORE = "api-core";
        /// <summary>
        /// 
        /// </summary>
        public const string API_SALARY = "api_salary";
        /// <summary>
        /// 
        /// </summary>
        public const string API_TIMEKEEPING = "api_timekeeping";

        /// <summary>
        /// 
        /// </summary>
        public static List<string> groupName = new List<string>()
        {
            API_CORE,API_SALARY,API_TIMEKEEPING
        };

        public static Dictionary<string, List<string>> DictionaryActionRoute = new Dictionary<string, List<string>>();

        public void Apply(ControllerModel controller)
        {
            throw new NotImplementedException();
        }
    }
}
