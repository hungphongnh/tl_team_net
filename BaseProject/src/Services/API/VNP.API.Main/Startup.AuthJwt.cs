﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using VNP.Salary.Framework.Config;

namespace VNP.API.Main
{
    public partial class Startup
    {
        /// <summary>
        /// Configures the JWT authentication service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="Configuration">The configuration.</param>
        /// <returns></returns>
        public void AddAuthJwt(IServiceCollection services, IConfiguration Configuration)
        {

            ConfigAudience Audience = new ConfigAudience();
            Configuration.GetSection("Audience").Bind(Audience);
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!  
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Audience.Secret)),

                // Validate the JWT Issuer (iss) claim  
                ValidateIssuer = true,
                ValidIssuer = Audience.Iss,

                // Validate the JWT Audience (aud) claim  
                ValidateAudience = true,
                ValidAudience = Audience.Aud,
                // Validate the token expiry  
                ValidateLifetime = true

            };

            services.AddAuthentication(
                options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }
            )
            .AddJwtBearer(
                o =>
                {
                    o.TokenValidationParameters = tokenValidationParameters;
                    o.RequireHttpsMetadata = false;
                }
            )
            .AddCookie(
                IdentityConstants.ApplicationScheme, 
                o =>
                {
                    o.LoginPath = new PathString("/api/v1.0/Account/Login/");
                    o.Events = new CookieAuthenticationEvents
                    {
                        OnValidatePrincipal = SecurityStampValidator.ValidatePrincipalAsync
                    };
                }
            )
            .AddCookie(
                IdentityConstants.ExternalScheme,
                o =>
                {
                    o.Cookie.Name = IdentityConstants.ExternalScheme;
                    o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                }
            )
            .AddCookie(
                IdentityConstants.TwoFactorRememberMeScheme,
                o => o.Cookie.Name = IdentityConstants.TwoFactorRememberMeScheme
            )
            .AddCookie(
                IdentityConstants.TwoFactorUserIdScheme,
                o =>
                {
                    o.Cookie.Name = IdentityConstants.TwoFactorUserIdScheme;
                    o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                }
            );
        }

    }
}
