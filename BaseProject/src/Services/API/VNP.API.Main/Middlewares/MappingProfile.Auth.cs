﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Collections.Generic;
using VNP.Auth.Entities;
using VNP.Auth.Entities.Dto;
using VNP.Salary.Framework.Autofac;
using VNP.Salary.Framework.Extension;
using VNP.Salary.Framework.Models;

namespace VNP.API.Main.Middlewares
{
    public partial class MappingProfile : Profile
    {
        public void AddAuthMappingProfile()
        {
            #region Sys_GroupFunction
            CreateMap<Auth_GroupFunction, Auth_GroupFunctionDto>().IgnoreNoMap();
            CreateMap<Auth_GroupFunctionDto, Auth_GroupFunction>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => ExtensionGuid.ToGuid(src.Id)))
                .ForMember(dest => dest.GroupId, opt => opt.MapFrom(src => ExtensionGuid.ToGuid(src.GroupId)))
                .ForMember(dest => dest.FunctionId, opt => opt.MapFrom(src => ExtensionGuid.ToGuid(src.FunctionId)))
                .ForMember(dest => dest.TenantId, opt => opt.MapFrom(src => ExtensionGuid.ToGuidOrNull(src.TenantId)))
                .ForMember(dest => dest.IsAllow, opt => opt.MapFrom(src => ExtensionCommon.ToBool(src.IsAllow)))
                .ForMember(dest => dest.UsedState, opt => opt.MapFrom(src => ExtensionCommon.ToInt(src.UsedState)))
                .ForMember(dest => dest.Orders, opt => opt.MapFrom(src => ExtensionCommon.ToInt(src.Orders)));
            #endregion


            CreateMap<Auth_GroupFunctionResourcesPaging, Auth_GroupFunctionResourcesPagingDto>()
                .ForMember(dest => dest.ResourceActions, opt => opt.MapFrom(src => JsonConvert.DeserializeObject<List<ResourceActions>>(src.ResourceActionsJson)));


            #region Mapping Sys_Tenant
            CreateMap<Auth_Tenant, Auth_TenantDto>().IgnoreNoMap();
            CreateMap<AspNetUsers, IdentityUserDto>();
            CreateMap<Auth_TenantDto, Auth_Tenant>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => ExtensionGuid.ToGuid(src.Id)));

            #endregion

        }
    }
}
