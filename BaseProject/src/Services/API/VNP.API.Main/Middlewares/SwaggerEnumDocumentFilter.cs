﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace VNP.API.Main.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter" />
    public class SwaggerEnumDocumentFilter : IDocumentFilter
    {
        /// <summary>
        /// Applies the specified swagger document.
        /// </summary>
        /// <param name="swaggerDoc">The swagger document.</param>
        /// <param name="context">The context.</param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            // add enum descriptions to result models
            foreach (var property in swaggerDoc.Components.Schemas.Where(x => x.Value?.Enum?.Count > 0))
            {
                if (property.Key.Contains("Nullable"))
                {
                    property.Value.Enum.Add(null);
                }

            }
            foreach (var property in swaggerDoc.Components.Schemas.Where(x => x.Value?.Enum?.Count > 0))
            {
                IList<IOpenApiAny> propertyEnums = property.Value.Enum;
                if (propertyEnums != null && propertyEnums.Count > 0)
                {
                    property.Value.Description = DescribeEnum(propertyEnums, property.Key);
                }
            }
            // add enum descriptions to input parameters 
            foreach (var pathItem in swaggerDoc.Paths.Values)
            {

                foreach (var oper in pathItem.Operations.Where(x => x.Value?.Parameters?.Count > 0))
                {
                    foreach (var param in oper.Value.Parameters)
                    {
                        if (param.Schema.Reference != null)
                        {
                            if (swaggerDoc.Components.Schemas.TryGetValue(param.Schema.Reference.Id, out OpenApiSchema schema))
                            {
                                IList<IOpenApiAny> propertyEnums = schema.Enum;
                                if (propertyEnums != null && propertyEnums.Count > 0)
                                {
                                    param.Description = DescribeEnum(propertyEnums, param.Schema.Reference.Id);
                                }
                            }
                        }
                    }
                }
            }
        }


        private void SetDescriptionEnumProperty(OpenApiSchema schemaRoot, OpenApiDocument swaggerDoc)
        {
            // add enum descriptions to input parameters 
            foreach (var item in schemaRoot.Properties)
            {
                if (item.Value.Reference != null)
                {
                    if (swaggerDoc.Components.Schemas.TryGetValue(item.Value.Reference.Id, out OpenApiSchema schema))
                    {
                        IList<IOpenApiAny> propertyEnums = schema.Enum;
                        if (propertyEnums != null && propertyEnums.Count > 0)
                        {
                            item.Value.Description = DescribeEnum(propertyEnums, item.Value.Reference.Id);
                        }
                        else
                        {
                            SetDescriptionEnumProperty(item.Value, swaggerDoc);
                        }
                    }
                }
            }
        }

        private Type GetTypeByName(string enumTypeName)
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .FirstOrDefault(x => x.Name == enumTypeName);
        }

        private string DescribeEnum(IList<IOpenApiAny> enums, string proprtyTypeName)
        {
            var enumType = GetTypeByName(proprtyTypeName);
            if (enumType == null && proprtyTypeName.Contains("Nullable"))
            {
                enumType = GetTypeByName(proprtyTypeName.Replace("Nullable", ""));
            }
            if (enumType != null && enumType.IsEnum)
            {
                List<string> enumDescriptions = new List<string>();
                enumDescriptions.Add("<br>Enum String = Value Int,// Description");
                foreach (OpenApiInteger enumOption in enums.Where(x => x != null))
                {
                    int enumInt = enumOption.Value;
                    enumDescriptions.Add($"<br>{enumType.GetEnumName(enumInt)} = {enumInt},// {GetEnumDescription(enumType, enumInt)}");
                }
                return string.Join("\n\r", enumDescriptions.ToArray());
            }
            return "";
        }

        private static string GetEnumDescription(Type enumType, int enumInt)
        {
            try
            {
                var nameEnum = enumType.GetEnumName(enumInt);
                FieldInfo fi = enumType.GetField(nameEnum);
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null &&
                    attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
                else
                {
                    return enumInt.ToString();
                }
            }
            catch
            {
                return enumInt.ToString();
            }
        }

    }
}
