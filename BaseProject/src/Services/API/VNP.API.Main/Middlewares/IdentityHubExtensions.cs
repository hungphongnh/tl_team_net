﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using VNP.Salary.Framework.Identity;
using VNP.Salary.Framework.Models;

namespace VNP.API.Main.Middlewares
{
    public static class IdentityHubExtensions
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TUser"></typeparam>
        /// <param name="services"></param>
        /// <param name="setupAction"></param>
        /// <returns></returns>
        private static IdentityBuilder AddIdentityHub<TUser>(this IServiceCollection services, Action<IdentityOptions> setupAction)
        where TUser : class
        {
            // Hosting doesn't add IHttpContextAccessor by default
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Identity services
            services.TryAddScoped<IUserValidator<TUser>, UserValidator<TUser>>();
            services.TryAddScoped<IPasswordValidator<TUser>, PasswordValidator<TUser>>();
            services.TryAddScoped<IPasswordHasher<TUser>, PasswordHasher<TUser>>();
            services.TryAddScoped<ILookupNormalizer, UpperInvariantLookupNormalizer>();

            // No interface for the error describer so we can add errors without rev'ing the interface
            services.TryAddScoped<IdentityErrorDescriber>();
            services.TryAddScoped<ISecurityStampValidator, SecurityStampValidator<TUser>>();
            services.TryAddScoped<IUserClaimsPrincipalFactory<TUser>, UserClaimsPrincipalFactory<TUser>>();
            services.TryAddScoped<IUserConfirmation<TUser>, DefaultUserConfirmation<TUser>>();


            if (setupAction != null)
            {
                services.Configure(setupAction);
            }

            return new IdentityBuilder(typeof(TUser), services);
        }

        /// <summary>
        /// Adds the identity hub.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IdentityBuilder AddIdentityHub(this IServiceCollection services)
        {

            services.AddSingleton<IUserStore<AspNetUsers>, UserStore>();
            services.AddSingleton<IRoleStore<AspNetRoles>, RoleStore>();
            services.AddSingleton<UserStore>();

            services.AddIdentityHub<AspNetUsers>
               (
                   opt =>
                   {
                       opt.Password.RequiredLength = 6;
                       opt.Password.RequireLowercase = false;
                       opt.Password.RequireUppercase = false;
                       opt.Password.RequireNonAlphanumeric = false;
                       opt.Password.RequireDigit = false;
                       opt.Lockout = new LockoutOptions
                       {
                           MaxFailedAccessAttempts = 10,
                           DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10),
                       };
                   }
               )
                .AddUserManager<UserManagerHub>()
               .AddSignInManager<SignInManagerHub>()
               .AddDefaultTokenProviders()
               .AddErrorDescriber<IdentityHubErrorCode>();

            return new IdentityBuilder(typeof(AspNetUsers), services);
        }

    }
}