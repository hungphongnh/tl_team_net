﻿using AutoMapper;
using System.Collections.Generic;

namespace VNP.API.Main.Middlewares
{
    public partial class MappingProfile : Profile
    {
        public MappingProfile()
        {
            AllowNullDestinationValues = true;

            this.AddCoreMappingProfile();
            this.AddAuthMappingProfile();
            this.AddSalaryMappingProfile();
            this.AddTimekeepingMappingProfile();
        }
    }

    public class AutoMapperConfiguration
    {
        public static List<Profile> RegisterMappings()
        {
            return new List<Profile> { new MappingProfile() };
        }
    }
}
