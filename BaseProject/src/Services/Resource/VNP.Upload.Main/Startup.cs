using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.IO;
using VNP.Salary.Framework.Interface;
using VNP.Salary.Framework.Logger;
using VNP.Upload.Main.Bussiness;
using VNP.Upload.Main.Bussiness.IBussiness;
using VNP.Upload.Main.Models;
using VNP.Upload.Main.Utilities.Files;
using VNP.Upload.Main.Utilities.Utilities.Files;

namespace VNP.Upload.Main
{
    public partial class Startup
    {
        #region property
        private string _policyNameAllow = "UploadAllowSpecificOrigins";
        private readonly string _serviceName = "Document-API";
        public IConfiguration Configuration { get; }
        #endregion

        #region constructor
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //DI Log
            services.AddSingleton<ILoggerManager, LoggerManager>();

            // Application services
            services.AddScoped<IFilesHelper, FilesHelper>();
            services.AddScoped<IFileUploadManager, FileUploadManager>();

            services.AddCors(options =>
            {
                options.AddPolicy(
                            name: _policyNameAllow,
                            builder =>
                            {
                                builder.WithOrigins(
                                    "https://salary.vnpost.vn",
                                    "http://salary.vnpost.vn",
                                    "https://localhost:44313",
                                    "https://api-salary.vnpost.vn",
                                    "http://api-salary.vnpost.vn",
                                    "https://localhost:44386",
                                    "https://document-salary.vnpost.vn",
                                    "http://document-salary.vnpost.vn",
                                    "https://salary-uat.vnpost.vn",
                                    "http://salary-uat.vnpost.vn",
                                    "https://luong.vnpost.vn",
                                    "http://luong.vnpost.vn"
                                    )
                                .AllowAnyHeader()
                                .AllowAnyMethod()
                                ;
                            }
                );
            });

            services.AddApiVersioning(/*o => o.ApiVersionReader = new HeaderApiVersionReader("api-version")*/);

            services.AddControllers().AddNewtonsoftJson();

            //Configure Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("api_upload", new OpenApiInfo
                {
                    Title = "Upload API",
                    Version = "1.0",
                    Description = "API for uploading file",
                });

                c.EnableAnnotations();
                c.CustomOperationIds((obj) => obj.GetHashCode().ToString());
            });

            //Add Identity User 
            //services.AddIdentityHub();
            //services.Configure<ConfigAudience>(Configuration.GetSection("Audience"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.ConfigureCustomExceptionMiddleware();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.ContentRootPath, $"{_serviceName }/Files")
                    ),
                RequestPath = $"/{_serviceName }/Files"
            });

            //if (env.IsDevelopment())
            //{
            //    // Enable middleware to serve generated Swagger as a JSON endpoint.

            //}

            app.UseSwagger();

            //Enable middleware to serve swagger - ui(HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                //c.RoutePrefix = string.Empty; //Configures swagger to load at app's root (http://localhost:<port>/)
                c.SwaggerEndpoint($"/swagger/api_upload/swagger.json", "api_upload");
                c.DocExpansion(DocExpansion.List);
                c.DisplayRequestDuration();
                c.EnableDeepLinking();
            });


            //app.UseHttpsRedirection();

            app.UseCors(_policyNameAllow);

            app.UseRouting();

            //app.UseAuthentication();
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
