﻿using Newtonsoft.Json;
using System.Net;

namespace VNP.Upload.Main.Models.CustomizeRespone
{

    public class CustomApiResponse
    {
        public bool IsError { get; set; }

        public int StatusCode { get; set; }

        public string Message { get; set; }

        public FileUploadResponse Result { get; set; }

        public CustomApiResponse()
        {
        }

        public CustomApiResponse(string message = "", bool isError = false, FileUploadResponse result = null)
        {
            this.IsError = isError;
            this.Message = message;
            this.StatusCode = isError ? (int)HttpStatusCode.Conflict : (int)HttpStatusCode.OK;
            this.Result = result;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
