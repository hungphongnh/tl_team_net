﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using VNP.Salary.Framework;

namespace VNP.Upload.Main.Models
{
    public class FileUploadByte : AuditEntity
    {
        [BindNever]
        public byte[] FileUpload { get; set; }
        public string NameFile { get; set; }
        public string NameTenant { get; set; }
    }
}

