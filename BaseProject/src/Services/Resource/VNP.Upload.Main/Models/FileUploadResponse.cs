﻿using System.Collections.Generic;

namespace VNP.Upload.Main.Models
{
    public class FileUploadResponse
    {
        public List<ViewDataUploadFilesResult> FileResults { get; set; } = new List<ViewDataUploadFilesResult>();
    }
}
