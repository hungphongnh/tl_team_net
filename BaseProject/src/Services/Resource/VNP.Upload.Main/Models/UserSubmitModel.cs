﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace VNP.Upload.Main.Models
{
    public class UserSubmitModel
    {
        [BindNever]
        public HttpContext HttpContext { get; set; }
        public List<IFormFile> Files { get; set; }
        public IFormFile File { get; set; }
    }
}
