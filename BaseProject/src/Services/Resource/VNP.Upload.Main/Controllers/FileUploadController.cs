﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VNP.Salary.Framework.Filter;
using VNP.Upload.Main.Bussiness.IBussiness;
using VNP.Upload.Main.Models;
using VNP.Upload.Main.Models.CustomizeRespone;

namespace VNP.Upload.Main.Controllers
{
    public class FileUploadController : DocumentControllerBase
    {
        #region property
        private readonly IFileUploadManager _fileUploadManager;
        #endregion

        public FileUploadController(IFileUploadManager fileUploadManager)
        {
            _fileUploadManager = fileUploadManager;
        }
        [HttpGet("{name}")]
        public async Task<MemoryStream> GetFile(string name)
        {
            var exportFile = new MemoryStream();
            return exportFile;
        }
        /// <summary>
        /// Chưa xử lý xóa file nếu user hủy container form chứa form upload file
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [DisableRequestSizeLimit]
        [HttpPost("Upload")]
        public async Task<CustomApiResponse> Upload(List<IFormFile> files)
        {
            UserSubmitModel userSubmitModel = new UserSubmitModel();
            userSubmitModel.Files = files;
            userSubmitModel.HttpContext = HttpContext;

            // how to check file folder if it does not exist in "constant"???

            var result = await _fileUploadManager.HandleUploadFile(userSubmitModel);
            return result;
        }

        //[DisableRequestSizeLimit]
        [HttpPost("UploadByte")]
        public async Task<CustomApiResponse> UploadByte([FromBody] FileUploadByte files) //public /*async*/ IActionResult
        {
            //HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            MemoryStream stream = new MemoryStream(files.FileUpload);
            IFormFile fileUpload = new FormFile(stream, 0, files.FileUpload.Length, "name", "fileName");

            UserSubmitModel userSubmitModel = new UserSubmitModel();
            userSubmitModel.File = fileUpload;
            userSubmitModel.HttpContext = HttpContext;

            // how to check file folder if it does not exist in "constant"???

            var result = await _fileUploadManager.HandleUploadOneFile(userSubmitModel, files);
            return result;
        }
        [HttpPost("UploadReport")]
        public async Task<CustomApiResponse> UploadReport([FromBody] FileUploadByte files) //public /*async*/ IActionResult
        {
            MemoryStream stream = new MemoryStream(files.FileUpload);
            IFormFile fileUpload = new FormFile(stream, 0, files.FileUpload.Length, "name", "fileName");

            UserSubmitModel userSubmitModel = new UserSubmitModel();
            userSubmitModel.File = fileUpload;
            userSubmitModel.HttpContext = HttpContext;

            var result = await _fileUploadManager.HandleUploadOneFile(userSubmitModel, files);
            return result;
        }
        [HttpPost("UploadImage")]
        public CustomApiResponse UploadImage([FromBody] FileUploadByte files)
        {
            MemoryStream stream = new MemoryStream(files.FileUpload);
            IFormFile fileUpload = new FormFile(stream, 0, files.FileUpload.Length, "name", "fileName");

            UserSubmitModel userSubmitModel = new UserSubmitModel();
            userSubmitModel.File = fileUpload;
            userSubmitModel.HttpContext = HttpContext;
            var result = _fileUploadManager.HandleUploadImage(files);
            return result;
        }

        /// <summary>
        /// Delete File When user cancel unsave form
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpDelete("DeleteFile")]
        public async Task<CustomApiResponse> DeleteFile(string deletePath, string fizeSize)
        {
            var result = await _fileUploadManager.HandleDeleteFile(deletePath, fizeSize);
            return result;
        }

        /// <summary>
        /// Delete File When user cancel unsave form
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpGet("Test")]
        public CustomApiResponse Test()
        {
            return new CustomApiResponse() { Message = "xxx" };
        }

    }

    //public ActionResult Show() => View(new FilesViewModel() { Files = _filesHelper.GetFileList().files });

    //public JsonResult GetFileList() => Json(_filesHelper.GetFileList());

    //[HttpGet("CheckConfigValue")]
    //public string CheckConfigValue([FromServices] IConfiguration config)
    //{
    //    HashSet<string> _allowedExtensions = config.GetSection("AllowedExtension").GetChildren().Select(x => x.Value).ToHashSet(StringComparer.OrdinalIgnoreCase);

    //    return "called";
    //}

    //[HttpGet("CheckApii")]
    //public string CheckApi()
    //{
    //    return "called";
    //}

}
