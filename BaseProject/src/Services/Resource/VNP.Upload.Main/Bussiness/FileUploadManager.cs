﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using VNP.Upload.Main.Models;
using VNP.Upload.Main.Utilities.Utilities.Files;
using VNP.Upload.Main.Utilities;
using ImageMagick;
using SkiaSharp;
using VNP.Upload.Main.Bussiness.IBussiness;
using VNP.Upload.Main.Models.CustomizeRespone;
using VNP.Upload.Main.Constant;

namespace VNP.Upload.Main.Bussiness
{
    public class FileUploadManager : IFileUploadManager
    {
        #region property
        private readonly IFilesHelper _filesHelper;
        private readonly IConfiguration _configuration;
        private readonly HashSet<string> _allowedExtensions;
        #endregion

        public FileUploadManager(IFilesHelper filesHelper, IConfiguration configuration)
        {
            _filesHelper = filesHelper;
            _configuration = configuration;
            _allowedExtensions = _configuration.GetSection("AllowedExtension").GetChildren().Select(x => x.Value).ToHashSet(StringComparer.OrdinalIgnoreCase);
        }

        public async Task<CustomApiResponse> HandleUploadFile(UserSubmitModel userSubmitModel)
        {
            var result = new FileUploadResponse();
            var partialFileName = userSubmitModel.HttpContext.Request.Headers["X-File-Name"];

            if (string.IsNullOrWhiteSpace(partialFileName)) await UploadWholeFileAsync(userSubmitModel, result);
            else UploadPartialFile(userSubmitModel, partialFileName);

            return new CustomApiResponse("Đã hoàn thành quá trình tải file lên server!", false, result);
        }

        public async Task<CustomApiResponse> HandleUploadOneFile(UserSubmitModel userSubmitModel, FileUploadByte files)
        {
            var result = new FileUploadResponse();
            var partialFileName = userSubmitModel.HttpContext.Request.Headers["X-File-Name"];

            if (string.IsNullOrWhiteSpace(partialFileName)) await UploadOneFile(userSubmitModel, files, result);
            else UploadPartialFile(userSubmitModel, partialFileName);

            return new CustomApiResponse("Đã hoàn thành quá trình tải file lên server!", false, result);
        }

        public async Task<CustomApiResponse> HandleDeleteFile(string deletePath, string fizeSize)
        {
            var isExisted = await _filesHelper.isDeletePathExisted(deletePath);

            if (!isExisted) return new CustomApiResponse("Đường dẫn file không tồn tại", true);

            var uploadServerFileSize = await _filesHelper.getDeleteFileSize(deletePath);

            if (!fizeSize.Equals(uploadServerFileSize.ToString())) return new CustomApiResponse("Khối lượng file size không trùng khớp!", true);

            var deleteResult = _filesHelper.DeleteFile(deletePath);
            return new CustomApiResponse($"{deleteResult}", false);
        }

        public async Task UploadOneFile(UserSubmitModel userSubmitModel, FileUploadByte files, FileUploadResponse result)
        {
            const int THUMB_WIDTH = 80;
            const int THUMB_HEIGHT = 80;
            const int NORMAL_IMAGE_MAX_WIDTH = 540;
            const string THUMBS_FOLDER_NAME = "thumbs";

            userSubmitModel.HttpContext.Request.Headers.TryGetValue("storagepathname", out var storagePath);
            //string storagePathStr = storagePath.ToString();
            string storagePathStr = files.NameFile;
            userSubmitModel.HttpContext.Request.Headers.TryGetValue("tenantname", out var tenantName);
            //string tenantNameStr = tenantName.ToString();
            string tenantNameStr = files.NameTenant;

            // Ensure the storage root exists.
            DateTime curDatetime = DateTime.Now;
            string sMonth = curDatetime.Month.ToString().PadLeft(2, '0'),
                   sDay = curDatetime.Day.ToString().PadLeft(2, '0'),
                   folderPath = $"{_filesHelper.StorageRootPath()}/{storagePathStr}/{curDatetime.Year}/{sMonth}/{sDay}",
                   newFileName = $"{storagePathStr}_{tenantNameStr}_{DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss")}";

            string shortFolderPath = $"/{FilePathConstant.SERVICE_NAME}/{FilePathConstant.FILE_DIR_PATH}/{storagePathStr}/{curDatetime.Year}/{sMonth}/{sDay}/{newFileName}";

            Directory.CreateDirectory(folderPath);

            var file = userSubmitModel.File;//userSubmitModel.Files.Count > 0 ? userSubmitModel.Files : userSubmitModel.File;


            var extension = Path.GetExtension(file.FileName);

            shortFolderPath = $"{shortFolderPath}{extension}";

            //if (!_allowedExtensions.Contains(extension)) return new CustomApiResponse($"Unsupported type: {extension}. The supported types are: {string.Join(", ", _allowedExtensions)}", true);

            var fullPath = Path.Combine(folderPath, $"{newFileName}{extension}");

            if (file.Length > 0L)
            {
                using (var fs = new FileStream(fullPath + ".xlsx", FileMode.Create))
                {
                    await file.CopyToAsync(fs);
                }

                //
                // Create an 80x80 thumbnail, and possibly resize the original if it exceeds an arbitrary max width.
                //

                var fileNameWithoutExtension = newFileName;
                var thumbName = $"{fileNameWithoutExtension}{THUMB_WIDTH}x{THUMB_HEIGHT}{extension}";
                var thumbPath = Path.Combine(_filesHelper.StorageRootPath(), THUMBS_FOLDER_NAME, thumbName);

                // Create the thumnail directory if it doesn't exist.
                Directory.CreateDirectory(Path.GetDirectoryName(thumbPath));

                // For GIFs, we have to use MagickNET. For JPEGs and PNGs, we can use SkiaSharp.
                if (IsGif(extension))
                {
                    using (var thumbStream = MagickNetResizeImage(fullPath, destWidthPx: 80, destHeightPx: 80))
                    using (var thumbFileStream = File.OpenWrite(thumbPath))
                    {
                        await thumbStream.CopyToAsync(thumbFileStream);
                    }

                    // If the image is wider than 540px, resize it so that it is 540px wide. Otherwise, upload a copy of the original.
                    using var originalImage = SKBitmap.Decode(fullPath);
                    if (originalImage.Width > NORMAL_IMAGE_MAX_WIDTH)
                    {
                        // Resize it so that the max width is 540px. Maintain the aspect ratio.
                        var newHeight = originalImage.Height * NORMAL_IMAGE_MAX_WIDTH / originalImage.Width;

                        var normalImageName = $"{fileNameWithoutExtension}{NORMAL_IMAGE_MAX_WIDTH}x{newHeight}{extension}";
                        var normalImagePath = Path.Combine(_filesHelper.StorageRootPath(), normalImageName);

                        using (var normalImgStream = MagickNetResizeImage(fullPath, NORMAL_IMAGE_MAX_WIDTH, newHeight))
                        using (var normalImgFileStream = File.OpenWrite(normalImagePath))
                        {
                            await normalImgStream.CopyToAsync(normalImgFileStream);
                        }
                    }
                }
                else if (IsPngJpeg(extension))
                {
                    using (var thumbStream = SkiaSharpResizeImage(fullPath, destWidthPx: 80, destHeightPx: 80))
                    using (var thumbFileStream = File.OpenWrite(thumbPath))
                    {
                        await thumbStream.CopyToAsync(thumbFileStream);
                    }

                    // If the image is wider than 540px, resize it so that it is 540px wide. Otherwise, upload a copy of the original.
                    using var originalImage = SKBitmap.Decode(fullPath);
                    if (originalImage.Width > NORMAL_IMAGE_MAX_WIDTH)
                    {
                        // Resize it so that the max width is 540px. Maintain the aspect ratio.
                        var newHeight = originalImage.Height * NORMAL_IMAGE_MAX_WIDTH / originalImage.Width;

                        var normalImageName = $"{fileNameWithoutExtension}{NORMAL_IMAGE_MAX_WIDTH}x{newHeight}{extension}";
                        var normalImagePath = Path.Combine(_filesHelper.StorageRootPath(), normalImageName);

                        using (var normalImgStream = SkiaSharpResizeImage(fullPath, NORMAL_IMAGE_MAX_WIDTH, newHeight))
                        using (var normalImgFileStream = File.OpenWrite(normalImagePath))
                        {
                            await normalImgStream.CopyToAsync(normalImgFileStream);
                        }
                    }
                }
                else
                {
                    // do nothing
                }

                result.FileResults.Add(UploadResult($"{newFileName}{Path.GetExtension(file.FileName)}", file.Length, fullPath, shortFolderPath));
            }
        }
        public CustomApiResponse HandleUploadImage(FileUploadByte files)
        {
            string storagePathStr = files.NameFile;
            string tenantNameStr = files.NameTenant;
            DateTime curDatetime = DateTime.Now;
            string sMonth = curDatetime.Month.ToString().PadLeft(2, '0'),
                   folderPath = $"{_filesHelper.StorageRootPath()}/{tenantNameStr}/{curDatetime.Year}/{sMonth}",
                   newFileName = $"{DateTime.Now.Ticks}_{storagePathStr}";
            Directory.CreateDirectory(folderPath);
            var fullPath = Path.Combine(folderPath, newFileName);
            string shortFolderPath = $"/{FilePathConstant.SERVICE_NAME}/{FilePathConstant.FILE_DIR_PATH}/{tenantNameStr}/{curDatetime.Year}/{sMonth}/{newFileName}";
            FileStream steam = File.Create(fullPath);
            steam.Write(files.FileUpload);
            steam.Flush();
            steam.Close();
            ViewDataUploadFilesResult viewDataUploadFilesResult = new ViewDataUploadFilesResult()
            {
                shortUrl = shortFolderPath,
            };
            var result = new FileUploadResponse();
            result.FileResults.Add(viewDataUploadFilesResult);
            return new CustomApiResponse() { IsError = false, Result = result };
        }
        public async Task UploadWholeFileAsync(UserSubmitModel userSubmitModel, FileUploadResponse result)
        {
            const int THUMB_WIDTH = 80;
            const int THUMB_HEIGHT = 80;
            const int NORMAL_IMAGE_MAX_WIDTH = 540;
            const string THUMBS_FOLDER_NAME = "thumbs";

            userSubmitModel.HttpContext.Request.Headers.TryGetValue("storagepathname", out var storagePath);
            string storagePathStr = storagePath.ToString();
            userSubmitModel.HttpContext.Request.Headers.TryGetValue("tenantname", out var tenantName);
            string tenantNameStr = tenantName.ToString();

            // Ensure the storage root exists.
            DateTime curDatetime = DateTime.Now;
            string sMonth = curDatetime.Month.ToString().PadLeft(2, '0'),
                   sDay = curDatetime.Day.ToString().PadLeft(2, '0'),
                   folderPath = $"{_filesHelper.StorageRootPath()}\\{storagePathStr}\\{curDatetime.Year}\\{sMonth}\\{sDay}",
                   newFileName = $"{storagePathStr}_{tenantNameStr}_{DateTime.Now.Ticks}";

            string shortFolderPath = $"\\{FilePathConstant.SERVICE_NAME}\\{FilePathConstant.FILE_DIR_PATH}\\{storagePathStr}\\{curDatetime.Year}\\{sMonth}\\{sDay}\\{newFileName}";

            Directory.CreateDirectory(folderPath);

            var files = userSubmitModel.Files;//userSubmitModel.Files.Count > 0 ? userSubmitModel.Files : userSubmitModel.File;

            foreach (var file in files)
            {
                var extension = Path.GetExtension(file.FileName);

                shortFolderPath = $"{shortFolderPath}{extension}";

                //if (!_allowedExtensions.Contains(extension)) return new CustomApiResponse($"Unsupported type: {extension}. The supported types are: {string.Join(", ", _allowedExtensions)}", true);

                var fullPath = Path.Combine(folderPath, $"{newFileName}{extension}");

                if (file.Length > 0L)
                {
                    using (var fs = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(fs);
                    }

                    //
                    // Create an 80x80 thumbnail, and possibly resize the original if it exceeds an arbitrary max width.
                    //

                    var fileNameWithoutExtension = newFileName;
                    var thumbName = $"{fileNameWithoutExtension}{THUMB_WIDTH}x{THUMB_HEIGHT}{extension}";
                    var thumbPath = Path.Combine(_filesHelper.StorageRootPath(), THUMBS_FOLDER_NAME, thumbName);

                    // Create the thumnail directory if it doesn't exist.
                    Directory.CreateDirectory(Path.GetDirectoryName(thumbPath));

                    // For GIFs, we have to use MagickNET. For JPEGs and PNGs, we can use SkiaSharp.
                    if (IsGif(extension))
                    {
                        using (var thumbStream = MagickNetResizeImage(fullPath, destWidthPx: 80, destHeightPx: 80))
                        using (var thumbFileStream = File.OpenWrite(thumbPath))
                        {
                            await thumbStream.CopyToAsync(thumbFileStream);
                        }

                        // If the image is wider than 540px, resize it so that it is 540px wide. Otherwise, upload a copy of the original.
                        using var originalImage = SKBitmap.Decode(fullPath);
                        if (originalImage.Width > NORMAL_IMAGE_MAX_WIDTH)
                        {
                            // Resize it so that the max width is 540px. Maintain the aspect ratio.
                            var newHeight = originalImage.Height * NORMAL_IMAGE_MAX_WIDTH / originalImage.Width;

                            var normalImageName = $"{fileNameWithoutExtension}{NORMAL_IMAGE_MAX_WIDTH}x{newHeight}{extension}";
                            var normalImagePath = Path.Combine(_filesHelper.StorageRootPath(), normalImageName);

                            using (var normalImgStream = MagickNetResizeImage(fullPath, NORMAL_IMAGE_MAX_WIDTH, newHeight))
                            using (var normalImgFileStream = File.OpenWrite(normalImagePath))
                            {
                                await normalImgStream.CopyToAsync(normalImgFileStream);
                            }
                        }
                    }
                    else if (IsPngJpeg(extension))
                    {
                        using (var thumbStream = SkiaSharpResizeImage(fullPath, destWidthPx: 80, destHeightPx: 80))
                        using (var thumbFileStream = File.OpenWrite(thumbPath))
                        {
                            await thumbStream.CopyToAsync(thumbFileStream);
                        }

                        // If the image is wider than 540px, resize it so that it is 540px wide. Otherwise, upload a copy of the original.
                        using var originalImage = SKBitmap.Decode(fullPath);
                        if (originalImage.Width > NORMAL_IMAGE_MAX_WIDTH)
                        {
                            // Resize it so that the max width is 540px. Maintain the aspect ratio.
                            var newHeight = originalImage.Height * NORMAL_IMAGE_MAX_WIDTH / originalImage.Width;

                            var normalImageName = $"{fileNameWithoutExtension}{NORMAL_IMAGE_MAX_WIDTH}x{newHeight}{extension}";
                            var normalImagePath = Path.Combine(_filesHelper.StorageRootPath(), normalImageName);

                            using (var normalImgStream = SkiaSharpResizeImage(fullPath, NORMAL_IMAGE_MAX_WIDTH, newHeight))
                            using (var normalImgFileStream = File.OpenWrite(normalImagePath))
                            {
                                await normalImgStream.CopyToAsync(normalImgFileStream);
                            }
                        }
                    }
                    else
                    {
                        // do nothing
                    }
                }

                result.FileResults.Add(UploadResult($"{newFileName}{Path.GetExtension(file.FileName)}", file.Length, fullPath, shortFolderPath));
            }
        }

        public void UploadPartialFile(UserSubmitModel userSubmitModel, string partialFileName) { throw new NotImplementedException(); }

        private MemoryStream SkiaSharpResizeImage(string localTempFilePath, int destWidthPx, int destHeightPx)
        {
            try
            {
                using var originalBmp = SKBitmap.Decode(localTempFilePath);
                using var scaledBmp = originalBmp.Resize(new SKImageInfo(destWidthPx, destHeightPx), SKFilterQuality.High);
                using var scaledImg = SKImage.FromBitmap(scaledBmp);

                SKEncodedImageFormat encodedImageFormat;
                int quality;

                var extension = Path.GetExtension(localTempFilePath).ToLower();

                switch (extension)
                {
                    case ".jpg":
                    case ".jpeg":
                        encodedImageFormat = SKEncodedImageFormat.Jpeg;
                        quality = 90;
                        break;

                    case ".png":
                        encodedImageFormat = SKEncodedImageFormat.Png;
                        quality = 100;
                        break;

                    // Skia doesn't support resizing GIFs. We use Magick.NET for that.
                    //case ".gif":
                    //    break;

                    default:
                        throw new NotSupportedException($"Unable to resize file: unsupported image type \"{extension}\"");
                }

                using var scaledImgData = scaledImg.Encode(encodedImageFormat, quality);

                var thumbnailStream = new MemoryStream();
                scaledImgData.SaveTo(thumbnailStream);
                thumbnailStream.Position = 0L;

                return thumbnailStream;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unhandled error trying to resize local image '{localTempFilePath}' to Width={destWidthPx}px, Height={destHeightPx}px", ex);
            }
        }

        private MemoryStream MagickNetResizeImage(string localTempFilePath, int destWidthPx, int destHeightPx)
        {
            try
            {
                // Read from file
                using (var collection = new MagickImageCollection(localTempFilePath))
                {
                    // This will remove the optimization and change the image to how it looks at that point
                    // during the animation. More info here: http://www.imagemagick.org/Usage/anim_basics/#coalesce
                    collection.Coalesce();

                    // Resize each image in the collection to a width of 200. When zero is specified for the height
                    // the height will be calculated with the aspect ratio.
                    foreach (MagickImage image in collection)
                    {
                        image.Resize(width: destWidthPx, height: destHeightPx);
                    }

                    // Save the result
                    var resizedImgStream = new MemoryStream();
                    collection.Write(resizedImgStream);
                    resizedImgStream.Position = 0L;

                    return resizedImgStream;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unhandled error trying to resize local image '{localTempFilePath}' to Width={destWidthPx}px, Height={destHeightPx}px", ex);
            }
        }

        /// <summary>
        /// Looks at the file's extension and returns true if this is a GIF; false otherwise.
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        private bool IsGif(string extension) => ".gif".Equals(extension, StringComparison.OrdinalIgnoreCase);

        private bool IsPngJpeg(string extension) => ".jpeg".Equals(extension, StringComparison.OrdinalIgnoreCase) || ".jpg".Equals(extension, StringComparison.OrdinalIgnoreCase) || ".png".Equals(extension, StringComparison.OrdinalIgnoreCase);

        /// <summary>
        ///     Create a new ViewDataUploadFilesResult object which will be sent to client
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileSizeInBytes"></param>
        /// <param name="fullPath"></param>
        /// <returns>ViewDataUploadFilesResult object that contains overal file's information</returns>
        private ViewDataUploadFilesResult UploadResult(string fileName, long fileSizeInBytes, string fullPath, string shortFolderPath)
        {
            var getType = MimeMapping.GetMimeMapping(fileName);

            return new ViewDataUploadFilesResult()
            {
                name = fileName,
                size = fileSizeInBytes,
                type = getType,
                url = fullPath,
                shortUrl = shortFolderPath,
                deleteUrl = fullPath,
                thumbnailUrl = _filesHelper.CheckThumb(getType, fileName),
                deleteType = _filesHelper.DeleteType(),
            };
        }

    }
}
