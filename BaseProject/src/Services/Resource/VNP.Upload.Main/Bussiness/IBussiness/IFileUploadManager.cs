﻿using System.Threading.Tasks;
using VNP.Upload.Main.Models;
using VNP.Upload.Main.Models.CustomizeRespone;

namespace VNP.Upload.Main.Bussiness.IBussiness
{
    public interface IFileUploadManager
    {
        public Task<CustomApiResponse> HandleUploadFile(UserSubmitModel userSubmitModel);
        public Task<CustomApiResponse> HandleDeleteFile(string file, string fizeSize);

        public Task UploadWholeFileAsync(UserSubmitModel userSubmitModel, FileUploadResponse result);
        public void UploadPartialFile(UserSubmitModel userSubmitModel, string partialFileName);

        public Task<CustomApiResponse> HandleUploadOneFile(UserSubmitModel userSubmitModel, FileUploadByte files);
        public CustomApiResponse HandleUploadImage(FileUploadByte files);
    }
}
