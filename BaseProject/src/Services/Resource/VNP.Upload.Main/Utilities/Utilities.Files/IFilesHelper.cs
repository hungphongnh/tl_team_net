﻿using System.Threading.Tasks;
using VNP.Upload.Main.Models;

namespace VNP.Upload.Main.Utilities.Utilities.Files
{
    public interface IFilesHelper
    {
        #region method
        public string DeleteFile(string deleteFilePath);
        public JsonFiles GetFileList();
        public ViewDataUploadFilesResult UploadResult(string FileName, int fileSize, string FileFullPath);
        public string CheckThumb(string type, string FileName);
        public string StorageRootPath();
        public string StorageTempPath();
        public string UrlBase();
        public string DeleteUrl();
        public string DeleteType();
        public ValueTask<bool> isDeletePathExisted(string deletePath);
        public ValueTask<long> getDeleteFileSize(string deletePath);
        #endregion
    }
}
