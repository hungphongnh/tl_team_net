﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VNP.Upload.Main.Constant;
using VNP.Upload.Main.Models;
using VNP.Upload.Main.Utilities.Utilities.Files;

namespace VNP.Upload.Main.Utilities.Files
{
    public class FilesHelper : IFilesHelper
    {
        #region property
        //private readonly IWebHostEnvironment _env;
        private readonly string _storageRootPath;
        private readonly string _storageTempPath;
        private string _rootDirectory;
        #endregion

        #region constructor
        public FilesHelper([FromServices] IConfiguration config)
        {
            _rootDirectory = config.GetValue<string>(WebHostDefaults.ContentRootKey) + $"/{FilePathConstant.SERVICE_NAME}";
            _storageRootPath = Path.Combine(_rootDirectory, FilePathConstant.FILE_DIR_PATH);
            _storageTempPath = Path.Combine(_rootDirectory, FilePathConstant.TEMP_PATH);
        }
        #endregion

        public string StorageRootPath() => _storageRootPath;

        public string StorageTempPath() => _storageTempPath;

        public string UrlBase() => FilePathConstant.URL_BASE;

        public string DeleteUrl() => FilePathConstant.DELETE_URL;

        public string DeleteType() => FilePathConstant.DELETE_TYPE;

        public string DeleteFile(string deleteFilePath)
        {
            File.Delete(deleteFilePath);
            return "Ok"; // succesMessage
        }

        public JsonFiles GetFileList()
        {
            string fullPath = Path.Combine(_storageRootPath);
            var results = new List<ViewDataUploadFilesResult>();

            if (Directory.Exists(fullPath))
            {
                DirectoryInfo dir = new DirectoryInfo(fullPath);

                foreach (FileInfo file in dir.GetFiles())
                {
                    results.Add(UploadResult(file.Name, unchecked((int)file.Length), file.FullName));
                }
            }

            return new JsonFiles(results);
        }

        public ViewDataUploadFilesResult UploadResult(string FileName, int fileSize, string FileFullPath)
        {
            string getType = MimeMapping.GetMimeMapping(FileFullPath);

            return new ViewDataUploadFilesResult()
            {
                name = FileName,
                size = fileSize,
                type = getType,
                url = FilePathConstant.URL_BASE + FileName,
                deleteUrl = FilePathConstant.DELETE_URL + FileName,
                thumbnailUrl = CheckThumb(getType, FileName),
                deleteType = FilePathConstant.DELETE_TYPE,
            };
        }

        public string CheckThumb(string type, string FileName)
        {
            var splited = type.Split('/');

            if (splited.Length != 2)
                return FilePathConstant.URL_BASE + "/thumbs/" + Path.GetFileNameWithoutExtension(FileName) + "80x80.jpg";

            string extansion = splited[1].ToLower();

            if (extansion.Equals("jpeg") || extansion.Equals("jpg") || extansion.Equals("png") || extansion.Equals("gif"))
                return $"{FilePathConstant.URL_BASE}thumbs/{Path.GetFileNameWithoutExtension(FileName)}80x80{Path.GetExtension(FileName)}";

            if (extansion.Equals("octet-stream"))
                return "/Content/Free-file-icons/48px/exe.png"; //Fix for exe files

            if (extansion.Contains("zip"))
                return "/Content/Free-file-icons/48px/zip.png"; //Fix for exe files

            return $"/Content/Free-file-icons/48px/{extansion}.png";
        }

        public ValueTask<bool> isDeletePathExisted(string deletePath)
        {
            return new ValueTask<bool>(File.Exists(deletePath));
        }

        public ValueTask<long> getDeleteFileSize(string deletePath)
        {
            return new ValueTask<long>(new FileInfo(deletePath).Length);
        }
    }
}


// This method is never called
//public void DeleteFiles(string pathToDelete)
//{

//    string path = HostingEnvironment.MapPath(pathToDelete);

//    Debug.WriteLine(path);
//    if (Directory.Exists(path))
//    {
//        DirectoryInfo di = new DirectoryInfo(path);
//        foreach (FileInfo fi in di.GetFiles())
//        {
//            File.Delete(fi.FullName);
//            Debug.WriteLine(fi.Name);
//        }

//        di.Delete(true);
//    }
//}

/* Disable because uploads work differently in ASP.NET Core MVC
public void UploadAndShowResults(HttpContextBase ContentBase, List<ViewDataUploadFilesResult> resultList)
{
    var httpRequest = ContentBase.Request;
    Debug.WriteLine(Directory.Exists(_storageTempPath));

    string fullPath = Path.Combine(_storageRootPath);
    Directory.CreateDirectory(fullPath);
    // Create new folder for thumbs
    Directory.CreateDirectory(fullPath + "/thumbs/");

    foreach (string inputTagName in httpRequest.Files)
    {

        var headers = httpRequest.Headers;

        var file = httpRequest.Files[inputTagName];
        Debug.WriteLine(file.FileName);

        if (string.IsNullOrEmpty(headers["X-File-Name"]))
        {

            UploadWholeFile(ContentBase, resultList);
        }
        else
        {

            UploadPartialFile(headers["X-File-Name"], ContentBase, resultList);
        }
    }
}


private void UploadWholeFile(HttpContextBase requestContext, List<ViewDataUploadFilesResult> statuses)
{

    var request = requestContext.Request;
    for (int i = 0; i < request.Files.Count; i++)
    {
        var file = request.Files[i];
        string pathOnServer = Path.Combine(_storageRootPath);
        var fullPath = Path.Combine(pathOnServer, Path.GetFileName(file.FileName));
        file.SaveAs(fullPath);

        //Create thumb
        string[] imageArray = file.FileName.Split('.');
        if (imageArray.Length != 0)
        {
            string extansion = imageArray[imageArray.Length - 1].ToLower();
            if (extansion != "jpg" && extansion != "png" && extansion != "jpeg") //Do not create thumb if file is not an image
            {

            }
            else
            {
                var ThumbfullPath = Path.Combine(pathOnServer, "thumbs");
                //string fileThumb = file.FileName + ".80x80.jpg";
                string fileThumb = Path.GetFileNameWithoutExtension(file.FileName) + "80x80.jpg";
                var ThumbfullPath2 = Path.Combine(ThumbfullPath, fileThumb);
                using (MemoryStream stream = new MemoryStream(File.ReadAllBytes(fullPath)))
                {
                    var thumbnail = new WebImage(stream).Resize(80, 80);
                    thumbnail.Save(ThumbfullPath2, "jpg");
                }

            }
        }
        statuses.Add(UploadResult(file.FileName, file.ContentLength, file.FileName));
    }
}



private void UploadPartialFile(string fileName, HttpContextBase requestContext, List<ViewDataUploadFilesResult> statuses)
{
    var request = requestContext.Request;
    if (request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
    var file = request.Files[0];
    var inputStream = file.InputStream;
    string patchOnServer = Path.Combine(_storageRootPath);
    var fullName = Path.Combine(patchOnServer, Path.GetFileName(file.FileName));
    var ThumbfullPath = Path.Combine(fullName, Path.GetFileName(file.FileName + "80x80.jpg"));
    ImageHandler handler = new ImageHandler();

    var ImageBit = ImageHandler.LoadImage(fullName);
    handler.Save(ImageBit, 80, 80, 10, ThumbfullPath);
    using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
    {
        var buffer = new byte[1024];

        var l = inputStream.Read(buffer, 0, 1024);
        while (l > 0)
        {
            fs.Write(buffer, 0, l);
            l = inputStream.Read(buffer, 0, 1024);
        }
        fs.Flush();
        fs.Close();
    }
    statuses.Add(UploadResult(file.FileName, file.ContentLength, file.FileName));
}
*/

// This method is never called
//public List<string> FilesList()
//{

//    List<string> Filess = new List<string>();
//    string path = HostingEnvironment.MapPath(serverMapPath);
//    Debug.WriteLine(path);
//    if (Directory.Exists(path))
//    {
//        DirectoryInfo di = new DirectoryInfo(path);
//        foreach (FileInfo fi in di.GetFiles())
//        {
//            Filess.Add(fi.Name);
//            Debug.WriteLine(fi.Name);
//        }

//    }
//    return Filess;
//}