﻿namespace VNP.Upload.Main.Constant
{
    public class FilePathConstant
    {
        #region Property
        public const string TEMP_PATH = @"";
        public const string SERVICE_NAME = @"Document-API";
        public const string FILE_DIR_PATH = @"Files";
        public const string URL_BASE = "/Files/";
        public const string DELETE_URL = "/FileUpload/DeleteFile/?file=";
        public const string DELETE_TYPE = "DELETE";
        #endregion

        //#region Property
        //public const string TEMP_PATH = @"somefiles";
        //public const string FILE_DIR_PATH = @"Files\somefiles";
        //public const string URL_BASE = "/Files/somefiles/";
        //public const string DELETE_URL = "/FileUpload/DeleteFile/?file=";
        //public const string DELETE_TYPE = "DELETE";
        //#endregion
    }
}
