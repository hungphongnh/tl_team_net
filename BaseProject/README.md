# VNPOST SALARY PROJECT
* Framework: SALARY
* Author: catmiu88 (hannv88@gmail.com)
* The project implements to calculate the salary for the VNPOST corporation
* https://vnpost.vn
* https://it.vnpost.vn
* -> VNP.Salary.All.sln: This file opens all projects in the product
* -> VNP.Services.Core.sln This file opens Core service projects
* -> VNP.Salary.API.sln This file opens the project website
* -> VNP.Services.Timekeeping.sln This file opens Timekeeping service projects
*
*
*